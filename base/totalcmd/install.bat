setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=tcmd%TOTALCMD_VERSION%%TOTALCMD_ARCH%.exe
set FILE2=INSTALL.CAB

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%TOOLS_BASE_DIR%\tools\totalcmd

:: extract specific files
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" %FILE2% -o%DESTINATION_DIR%
%BIN%\7za.exe x "%DESTINATION_DIR%\%FILE2%" -o%DESTINATION_DIR%
del %DESTINATION_DIR%\%FILE2%


:: copy ini files
:: copy template with bat scripts and ini file
xcopy /s /q /i %~dp0template %DESTINATION_DIR%
