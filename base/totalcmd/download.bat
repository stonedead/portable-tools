setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


:: https://totalcommander.ch/win/tcmd921ax32.exe
set FILE=tcmd%TOTALCMD_VERSION%%TOTALCMD_ARCH%.exe
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)

set BASE_URL=https://totalcommander.ch/win
set URL=%BASE_URL%/%FILE%
%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"