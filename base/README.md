# Portable Basic Tools

## Introduction

These tools are my basic setup starting with total commander as file manager and adding support for basic file formats.


## List of Tools


### Total Commander

Total Commander is closed source and comes with a price tag. 
The alternative doublecmd (open source and cross-platform) is getting better.

It supports portable configuration, and portable association, see `totalcmd\template`.



see https://www.ghisler.com/

### Autohotkey

This tool already used for some setup tasks in this repository, while the language is ugly and unpleasant the capabilities are almost unlimited.

see https://autohotkey.com/

### Notepad3

There are many editors available, notepad3 was picked for fast startup and slim footprint (Notepad++ might be another choice).

see https://github.com/rizonesoft/Notepad3

### IrfanView

Lightweight and fast image viewer. Closed source freeware.

see https://www.irfanview.com/


### SumatraPDF

Lightweight and fast PDF viewer, might have some limits with specific functions like forms or signatures.

see https://www.sumatrapdfreader.org/

### XMPlay

Lightweight and fast audio player, supports all requirements for serious sound quality (ASIO, FLAC, etc.).
Standard skin is not that great, my replacement skin is silk (small and simple).

XMPlay is closed source but freeware. Don't know any proper open source alternative.

see http://www.un4seen.com/xmplay.html

## Installation

install binaries:

```
base\autohotkey\install.bat
base\irfanview\install.bat
base\notepad3\install.bat
base\sumatrapdf\install.bat
base\totalcmd\install.bat
base\xmplay\install.bat
```

create desktop shortcut with: `base\totalcmd\install_shortcut.bat`

This can be done later by using total commander and press enter on `shortcut.ahk` in binary in installation directory.