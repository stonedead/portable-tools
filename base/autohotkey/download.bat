setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

:: https://autohotkey.com/download/
:: https://autohotkey.com/download/1.1/AutoHotkey_1.1.30.01.zip
set FILE=AutoHotkey_%AUTOHOTKEY_VERSION%.zip
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)

set BASE_URL=https://autohotkey.com/download/1.1/
set URL=%BASE_URL%/%FILE%
%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"