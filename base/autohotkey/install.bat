setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=AutoHotkey_%AUTOHOTKEY_VERSION%.zip

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%TOOLS_BASE_DIR%\tools\autohotkey

:: extract specific files
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" AutoHotkeyU32.exe -o%DESTINATION_DIR%
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" AutoHotkey.chm -o%DESTINATION_DIR%

:: rename file?
:: ren %DESTINATION_DIR%\AutoHotkeyU32.exe AutoHotkey.exe
copy %DESTINATION_DIR%\AutoHotkeyU32.exe %DESTINATION_DIR%\AutoHotkeyU32c.exe

:: create console version
%BIN%\AutoHotkeyU32c.exe %~dp0make_console.ahk %DESTINATION_DIR%\AutoHotkeyU32c.exe C

