:: product versions defaults

:: https://www.ghisler.com/
set TOTALCMD_VERSION=1151
:: set TOTALCMD_ARCH=x32
set TOTALCMD_ARCH=x64

:: https://github.com/doublecmd/doublecmd/releases/
set DOUBLECMD_VERSION=1.1.22
:: set DOUBLECMD_ARCH=i386-win32
set DOUBLECMD_ARCH=x86_64-win64

:: https://www.autohotkey.com/download/
:: https://www.autohotkey.com/download/1.1/version.txt
set AUTOHOTKEY_VERSION=1.1.37.02
:: set AUTOHOTKEY_VERSION=2.0.19

:: https://www.irfanview.info/
set IVIEW_VERSION=470

:: https://www.rizonesoft.com/downloads/notepad3/
set NOTEPAD3_VERSION=6.24.1221.1

:: http://www.un4seen.com/xmplay.html
set XMPLAY_VERSION=40

:: https://www.sumatrapdfreader.org/download-free-pdf-viewer.html
set SUMATRAPDF_VERSION=3.5.2
set SUMATRAPDF_ARCH=64
if %PROCESSOR_ARCHITECTURE%==ARM64 set SUMATRAPDF_ARCH=arm64