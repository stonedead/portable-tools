setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=SumatraPDF-%SUMATRAPDF_VERSION%-%SUMATRAPDF_ARCH%.zip

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%TOOLS_BASE_DIR%\media\pdf\

:: extract specific files
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o%DESTINATION_DIR%

:: rename file (new with 3.2) SumatraPDF-3.2-32.exe
ren %DESTINATION_DIR%\SumatraPDF-%SUMATRAPDF_VERSION%-%SUMATRAPDF_ARCH%.exe SumatraPDF.exe

:: copy ini file
copy /Y %~dp0\template\SumatraPDF-settings.txt %DESTINATION_DIR%

