setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


:: https://www.sumatrapdfreader.org/dl/SumatraPDF-3.1.2.zip
:: https://www.sumatrapdfreader.org/dl/rel/SumatraPDF-3.3.3-64.zip
:: https://www.sumatrapdfreader.org/dl/rel/3.4.1/SumatraPDF-3.4.1-64.zip
:: https://www.sumatrapdfreader.org/dl/rel/3.5.2/SumatraPDF-3.5.2-arm64.zip
set FILE=SumatraPDF-%SUMATRAPDF_VERSION%-%SUMATRAPDF_ARCH%.zip
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)

set BASE_URL=https://www.sumatrapdfreader.org/dl/rel
set URL=%BASE_URL%/%SUMATRAPDF_VERSION%/%FILE%
%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"