setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=xmplay%XMPLAY_VERSION%.zip

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%TOOLS_BASE_DIR%\media\audio\XMPlay

:: extract all zips
for %%P in (xmplay%XMPLAY_VERSION% xmp-flac xmp-dsd xmp-asio xmp-wasapi) do (
  call :extract %%P
)

:: exctract silk (single version)
%BIN%\7za.exe x "%DOWNLOAD_DIR%\Silk.zip" Silk.xmpskin -o%DESTINATION_DIR%

:: copy ini files
:: copy template with bat scripts and ini file
xcopy /s /q /i %~dp0template %DESTINATION_DIR%

goto :eof

:extract
set FILE=%~1.zip
:: extract specific files
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o%DESTINATION_DIR%

exit /B 0


