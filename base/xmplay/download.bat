setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


:: http://www.un4seen.com/files/XXXX.zip
set BASE_URL=http://www.un4seen.com/files
:: set BASE_URL=http://uk.un4seen.com/files

for %%P in (xmplay%XMPLAY_VERSION% xmp-flac xmp-dsd xmp-asio xmp-wasapi) do (
  call :download %%P /
)

:: http://www.un4seen.com/files/skin/Silk.zip
call :download Silk /skin/

goto :eof

: download
set FILE=%~1.zip
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)

set URL=%BASE_URL%%~2%FILE%
%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"

exit /B 0
