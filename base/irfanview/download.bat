setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set BASE_URL=https://www.irfanview.info

:: https://www.irfanview.info/files/iview452.zip
:: https://www.irfanview.info/files/iview452_plugins.zip
:: https://www.irfanview.com/skins/irfanview_skin_grosberg.zip

call :download iview%IVIEW_VERSION%.zip files
call :download iview%IVIEW_VERSION%_plugins.zip files
call :download irfanview_skin_grosberg.zip skins

goto :eof

:download
if exist "%DOWNLOAD_DIR%\%~1" (
  echo file exits: %~1
  exit /B 0
)
set URL=%BASE_URL%/%~2/%~1
%BIN%\wget.exe %URL% --header="Referer: %URL%" -P "%DOWNLOAD_DIR%"

exit /B 0