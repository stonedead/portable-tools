setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=iview%IVIEW_VERSION%.zip
set FILE_PLUGINS=iview%IVIEW_VERSION%_plugins.zip
set FILE_SKIN=irfanview_skin_grosberg.zip

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%TOOLS_BASE_DIR%\media\image\IView

:: extract binary package
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o%DESTINATION_DIR%

:: extract skin (overwrite)
%BIN%\7za.exe x -y "%DOWNLOAD_DIR%\%FILE_SKIN%" -o%DESTINATION_DIR%\Toolbars

:: extract webp file
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE_PLUGINS%" WebP.dll -o%DESTINATION_DIR%\Plugins

:: extract svg file
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE_PLUGINS%" SVG.dll -o%DESTINATION_DIR%\Plugins

:: copy ini file
copy /Y %~dp0\template\i_view32.ini %DESTINATION_DIR%

