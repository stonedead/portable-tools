setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=doublecmd-%DOUBLECMD_VERSION%.%DOUBLECMD_ARCH%.zip

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%TOOLS_BASE_DIR%\tools\doublecmd

:: extract files, doublecmd path in zip
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o%TOOLS_BASE_DIR%\tools

:: copy config files
xcopy /s /q /i %~dp0template %DESTINATION_DIR%\settings
