setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=doublecmd-%DOUBLECMD_VERSION%.%DOUBLECMD_ARCH%.zip
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)

:: https://github.com/doublecmd/doublecmd/releases/download/v1.0.3/doublecmd-1.0.3.i386-win32.zip
:: https://github.com/doublecmd/doublecmd/releases/download/v1.0.3/doublecmd-1.0.3.x86_64-win64.zip
set DOUBLECMD_URL=https://github.com/doublecmd/doublecmd/releases/download/v%DOUBLECMD_VERSION%/%FILE%

%BIN%\wget.exe "%DOUBLECMD_URL%" -P "%DOWNLOAD_DIR%"