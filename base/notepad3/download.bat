setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


:: https://www.rizonesoft.com/genesis/Notepad3/Notepad3_5.19.108.1602.zip
:: https://www.rizonesoft.com/shkarko/Notepad3/Notepad3_5.19.630.2381.zip
:: https://www.rizonesoft.com/software/notepad3/Notepad3_5.20.414.1.zip
:: https://www.rizonesoft.com/download/3943/ zip file with both versions
:: https://www.rizonesoft.com/mimnaa/10910/
:: https://downloads.rizonesoft.com/Notepad3/Notepad3_6.24.1221.1_Portable.zip

set FILE=Notepad3_%NOTEPAD3_VERSION%_Portable.zip
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)

set BASE_URL=https://downloads.rizonesoft.com/Notepad3
set URL=%BASE_URL%/%FILE%
%BIN%\wget.exe %URL% --content-disposition -P "%DOWNLOAD_DIR%"