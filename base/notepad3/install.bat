setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=Notepad3_%NOTEPAD3_VERSION%_Portable.zip
set FILE2=Notepad3_%NOTEPAD3_VERSION%_x86_Portable.zip

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%TOOLS_BASE_DIR%\tools\notepad3

:: extract specific files
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" %FILE2% -o%DESTINATION_DIR%
%BIN%\7za.exe x %DESTINATION_DIR%\%FILE2% -o%DESTINATION_DIR%
del %DESTINATION_DIR%\%FILE2%

:: copy ini file
copy /Y %~dp0\template\Notepad3.ini %DESTINATION_DIR%

