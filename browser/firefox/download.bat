setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


set FILE=Firefox Setup %FIREFOX_VERSION%.exe
set FILE_ARCH=Firefox Setup %FIREFOX_VERSION%_%FIREFOX_ARCH%.exe

if exist "%DOWNLOAD_DIR%\%FILE_ARCH%" (
  echo file exits: %FILE_ARCH%
  goto :DOWNLOAD_STYLE
)

:: url encoded file name
set FILE_URL=Firefox%%20Setup%%20%FIREFOX_VERSION%.exe

set BASE_URL=http://ftp.mozilla.org/pub/firefox/releases
:: TODO language
set URL=%BASE_URL%/%FIREFOX_VERSION%/%FIREFOX_ARCH%/en-US/%FILE_URL%
%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"

ren "%DOWNLOAD_DIR%\%FILE%" "%FILE_ARCH%"

:DOWNLOAD_STYLE
set STYLE_FILE=Firefox-%FIREFOX_STYLE_FILE%-%FIREFOX_STYLE_VERSION%.zip
if exist "%DOWNLOAD_DIR%\%STYLE_FILE%" (
  echo file exits: %STYLE_FILE%
  exit /B 0
)

%BIN%\wget.exe %FIREFOX_STYLE_URL%/%FIREFOX_STYLE_FILE%.zip -P "%DOWNLOAD_DIR%"
ren "%DOWNLOAD_DIR%\%FIREFOX_STYLE_FILE%.zip" "%STYLE_FILE%"