@echo off
setlocal
:: destination directories
set APP=%~dp0app
set PROFILE=%~dp0profile

:::: Command Line Options (disable with :: prefix) ::::

:: select profile directory
set ARGS=-profile %PROFILE%

:: enable multiple instances (avoiding socket lock)
set ARGS=%ARGS% -no-remote 

:: private mode
:: set ARGS=%ARGS% -private-window 

:: start with console (allows to check remote/home calls during startup)
:: set ARGS=%ARGS% -jsconsole

:: this environment "enables" services.settings.server to disable home calling.
:: see https://bugzilla.mozilla.org/show_bug.cgi?id=1598562
set XPCSHELL_TEST_PROFILE_DIR=fuck-mozilla-settings
:: new switch for FF99+
set MOZ_REMOTE_SETTINGS_DEVTOOLS=1
start %APP%\firefox.exe %ARGS%