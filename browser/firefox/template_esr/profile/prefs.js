// don't ask for default browser
user_pref("browser.shell.checkDefaultBrowser", false);
// don't warn when using about:config
user_pref("general.warnOnAboutConfig", false);
// FF71: using a new key is great - thank you so much.
user_pref("browser.aboutConfig.showWarning", false);
// don't warn close multiple tabs
user_pref("browser.tabs.warnOnClose", false);

// disable first run crap
user_pref("browser.startup.homepage_override.mstone", "ignore");

// disable win10 "welcome" 
user_pref("browser.usedOnWindows10", true);

// disable reader mode "popup" 
user_pref("browser.reader.detectedFirstArticle", true);

// default font (for "raw" html), get rid of serifs
user_pref("font.name.serif.x-western", "Verdana");
user_pref("font.size.variable.x-western", 14);
// default "Courier New" is too "thin" for hidpi
user_pref("font.name.monospace.x-western", "Consolas");

// override download directory
// user_pref("browser.download.dir", "d:\\download");
// user_pref("browser.download.folderList", 2);

// default search engine. requires profile/searchplugins/google-encrypted.xml
// this is not working
// user_pref("browser.search.defaultenginename", "Google encrypted");

// this is set by gui, but it only avoids upload
user_pref("datareporting.healthreport.uploadEnabled", false);
// disable service 
user_pref("datareporting.healthreport.service.enabled", false);
// telemetry
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.enabled", false);
user_pref("toolkit.telemetry.server", "data:,");
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
user_pref("browser.ping-centre.telemetry", false);

// crash reports
user_pref("breakpad.reportURL", "");
user_pref("browser.tabs.crashReporting.sendReport", false);

// shield/normandy/heartbeat
user_pref("extensions.shield-recipe-client.enabled", false);
user_pref("extensions.shield-recipe-client.api_url","");
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("app.normandy.enabled", false);
user_pref("app.normandy.api_url", "");
user_pref("messaging-system.rsexperimentloader.enabled", false);
user_pref("app.shield.optoutstudies.enabled", false);

// no drm (options - content - drm content - play drm content)
user_pref("media.eme.enabled", false);
user_pref("media.gmp-eme-adobe.enabled", false);

// safe browsing
user_pref("browser.safebrowsing.enabled", false);
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);
user_pref("browser.safebrowsing.downloads.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.enabled", false);
// need to disable urls, switches above don't avoid remote calls.. WTF?
user_pref("browser.safebrowsing.provider.mozilla.gethashURL", "");
user_pref("browser.safebrowsing.provider.mozilla.updateURL", "");
user_pref("browser.safebrowsing.downloads.remote.url", "");


// automatic updates (search but ask)
user_pref("app.update.auto", false);
user_pref("extensions.update.autoUpdateDefault", false);
user_pref("browser.search.update", false);
user_pref("extensions.getAddons.cache.enabled", false);

// avoid silent update or install of "any" system addon
user_pref("extensions.systemAddon.update.enabled", false);

// block new requests, TODO clarify.
user_pref("extensions.blocklist.enabled", false);

// newtab/tiles.services
user_pref("browser.newtabpage.enabled", false);
user_pref("browser.newtabpage.enhanced", false);
user_pref("browser.newtab.preload", false);
user_pref("browser.newtabpage.introShown", true);
user_pref("browser.newtabpage.directory.ping", "");
user_pref("browser.newtabpage.directory.source", "");
user_pref("browser.newtabpage.activity-stream.enabled", false);
user_pref("browser.newtabpage.activity-stream.tippyTop.service.endpoint", "");

// contile FF92?
user_pref("browser.topsites.contile.enabled", false);
user_pref("browser.topsites.contile.endpoint", "");

// homepage etc.
// disable requests to snippets.cdn.mozilla.net
user_pref("browser.aboutHomeSnippets.updateUrl", "");

// need to disable activity-stream/snippet shit AGAIN (FF64+) - this is a toilet.
// see also https://github.com/ghacksuserjs/ghacks-user.js/issues/528
user_pref("browser.newtabpage.activity-stream.feeds.asrouterfeed", false);
// options - General - Recommend extensions as you browse (Contextual Feature Recommender)
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr", false);
//user_pref("browser.newtabpage.activity-stream.asrouter.providers.cfr", "");
// with FF74 (?), you need to opt out of this sh*t again - thank you!
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false);

// these are useless?
//user_pref("browser.newtabpage.activity-stream.feeds.snippets", false);
//user_pref("browser.library.activity-stream.enabled", false);
//user_pref("browser.newtabpage.activity-stream.disableSnippets", true);
//user_pref("browser.newtabpage.activity-stream.asrouter.providers.snippets", "");
//user_pref("browser.newtabpage.activity-stream.asrouter.providers.onboarding", "");

// disable firefox start page
user_pref("browser.startup.homepage", "about:blank");
// 3=show previous windows on startup, 1=home page (default), 0=blank
// user_pref("browser.startup.page", 3);

// heartbeat https://wiki.mozilla.org/Advocacy/heartbeat
user_pref("browser.selfsupport.url", "");

// hello
user_pref("loop.enabled", false);
// do not download OpenH264 codec
user_pref("media.gmp-gmpopenh264.enabled", false);

// disable WebRTC
user_pref("media.peerconnection.enabled", false);

// beacon?
user_pref("beacon.enabled", false);

// media casting?
user_pref("browser.casting.enabled", false);

// pocket
user_pref("browser.pocket.enabled", false);
user_pref("browser.toolbarbuttons.introduced.pocket-button", true);

// location
user_pref("geo.enabled", false);
// avoid requests to https://location.services.mozilla.com/v1/country 
user_pref("browser.search.geoip.url", "");
// need to disable "again"
user_pref("geo.provider-country.network.url", "");
// and again
user_pref("browser.region.network.url", "");

// no tracking protection (use instead: ublock origin)
user_pref("privacy.trackingprotection.enabled", false);
user_pref("privacy.trackingprotection.pbmode.enabled", false);

// preload
user_pref("network.prefetch-next", false);
user_pref("network.dns.disablePrefetch", true);

// search as you type
user_pref("browser.search.suggest.enabled", false);

// referer
user_pref("network.http.referer.spoofSource", true);

// no disk cache
user_pref("browser.cache.disk.enable", false);

// disable web workers (see about:serviceworkers)
user_pref("dom.serviceWorkers.enabled", false);

// disable captive crap...
user_pref("captivedetect.canonicalURL", "");
user_pref("network.captive-portal-service.enabled", false);
// remove more "detection"
user_pref("network.connectivity-service.enabled", false);

// enable compact light theme
user_pref("lightweightThemes.selectedThemeID", "firefox-compact-light@mozilla.org");
// enable compact dark theme
// user_pref("lightweightThemes.selectedThemeID", "firefox-compact-dark@mozilla.org");
user_pref("browser.uidensity", 1);
user_pref("lightweightThemes.usedThemes", "[]");
// disable some buttons...
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"PersonalToolbar\":[\"personal-bookmarks\"],\"nav-bar\":[\"back-button\",\"forward-button\",\"developer-button\",\"stop-reload-button\",\"urlbar-container\",\"downloads-button\",\"library-button\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"toolbar-menubar\":[\"menubar-items\"]},\"seen\":[\"developer-button\",\"webide-button\"],\"dirtyAreaCache\":[\"PersonalToolbar\",\"nav-bar\",\"TabsToolbar\",\"toolbar-menubar\"],\"currentVersion\":12,\"newElementCount\":4}");


// no crappy search buttons (not working anymore - thank you)
user_pref("browser.urlbar.oneOffSearches", false);

// FF77 switch was removed, need to switch off "individually"
// see https://bugzilla.mozilla.org/show_bug.cgi?id=1628926
// note: it is not possible to remove engines added with policies.json
user_pref("browser.search.hiddenOneOffs", "Google,Bing,Amazon.com,DuckDuckGo,eBay,Twitter,Wikipedia (en),Wikipedia DE,Wikipedia EN,Google Encrypted,Google Lucky,Google Image,Google Maps");
// fix for en_GB "edition"
// user_pref("browser.search.hiddenOneOffs", "Google,Amazon.co.uk,Bing,Chambers (UK),DuckDuckGo,eBay,Wikipedia (en)");

// megabar crap, topsites
user_pref("browser.urlbar.openViewOnFocus", false);
user_pref("browser.urlbar.suggest.topsites", false);

// avoid crappy introduction
user_pref("extensions.screenshots.disabled", true);

// no onboarding, shove that in your foxy ass..
user_pref("browser.onboarding.notification.finished", true);
// not required user_pref("browser.onboarding.hidden", true);
user_pref("browser.onboarding.enabled", false);

// no need for extra search field. use "keywords"
user_pref("browser.search.widget.inNavBar", false);

// fix ctrl-tab
user_pref("browser.ctrlTab.previews",true);
// FF 91.5.0
user_pref("browser.ctrlTab.sortByRecentlyUsed", true);

// promo crap, beta only?
user_pref("devtools.devedition.promo.shown",true);

// When Firefox starts: Show your windows and tabs from last time
user_pref("browser.startup.page", 3);


// ff connects to wss://push.services.mozilla.com/
user_pref("dom.push.connection.enabled", false);
user_pref("dom.push.enabled", false);
user_pref("dom.push.serverURL", "");
user_pref("dom.webnotifications.enabled", false);

// disable "remote settings", this polls every 24h for new things (mainly blocklist)
user_pref("services.settings.server", "data:,");

// introduced with FF66: block auto play per default
user_pref("media.autoplay.default", 1);

// suppress message "install Microsoft's Media Feature Pack" for windows 10 "N" editions
user_pref("media.decoder-doctor.notifications-allowed", "");

// FF67: account (aka sync) button
user_pref("identity.fxaccounts.toolbar.enabled", false);

// FF68: disable "enterprise" MITM
user_pref("security.certerrors.mitm.auto_enable_enterprise_roots", false);
user_pref("security.enterprise_roots.enabled", false);

// FF86? more helping
user_pref("security.certerrors.mitm.priming.enabled", false);

// disable automatic update check for video codec (gmp-gmpopenh264, gmp-widevinecdm)
user_pref("media.gmp-manager.url", "");

// enable spell checking also for single line input (why is this NOT default?)
user_pref("layout.spellcheckDefault", 2);

// FF75 whats new "button"
user_pref("browser.messaging-system.whatsNewPanel.enabled", false);

// FF80 helps with reading "reduced motion" from OS
user_pref("ui.prefersReducedMotion", 0);

// disable bits
user_pref("app.update.BITS.enabled", false);

// FF106 disable firefox-view button
user_pref("browser.tabs.firefox-view", false);

// FF 108. do NOT show bookmark bar on newtab page
user_pref("browser.toolbars.bookmarks.visibility", "never");

// some console settings
user_pref("devtools.webconsole.timestampMessages", true);
user_pref("devtools.browserconsole.contentMessages", true);
user_pref("devtools.browserconsole.filter.net", true);
user_pref("devtools.browserconsole.filter.netxhr", true);
// don't lecture me
user_pref("devtools.webconsole.input.editorOnboarding", false);

// FF 113 elastic overscroll was activated
user_pref("apz.overscroll.enabled", false);

user_pref("media.videocontrols.picture-in-picture.video-toggle.enabled", false);

// FF?? allow manual adding of a search engine
user_pref("browser.urlbar.update2.engineAliasRefresh", true);