setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set DESTINATION_DIR=%BROWSER_BASE_DIR%\%FIREFOX_PATH%_esr

:: create shortcut
%BIN%\AutoHotkeyU32c.exe %DESTINATION_DIR%\shortcut.ahk
