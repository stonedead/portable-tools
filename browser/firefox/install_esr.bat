setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


set DESTINATION_DIR=%BROWSER_BASE_DIR%\%FIREFOX_PATH%_esr

:: rmdir /s /q %DESTINATION_DIR%

set FILE=Firefox Setup %FIREFOX_ESR_VERSION%esr_%FIREFOX_ARCH%.exe

if not exist "%DOWNLOAD_DIR%\%FILE%" call %~dp0download_esr.bat

:: extract "core" from installer to %DESTINATION_DIR%\app
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o"%DESTINATION_DIR%"
del "%DESTINATION_DIR%\setup.exe"
ren "%DESTINATION_DIR%\core" app

:: copy template files to destination
xcopy %~dp0template_esr %DESTINATION_DIR% /Q /E /I

:: "replace" plugin url in policies.json (this will overwrite already copied file)
:: %BIN%\AutoHotkeyU32c.exe %BIN%\replace_env.ahk %~dp0template_esr\app\distribution\policies.json %DESTINATION_DIR%\app\distribution\policies.json

:: install style
set STYLE_FILE=Firefox-%FIREFOX_STYLE_ESR_FILE%-%FIREFOX_STYLE_VERSION%.zip

if not exist "%DOWNLOAD_DIR%\%STYLE_FILE%" call %~dp0download_esr.bat

:: %BIN%\7za.exe x "%DOWNLOAD_DIR%\%STYLE_FILE%" -o"%DESTINATION_DIR%\profile"
:: tar --strip-components=1 -x -f "%DOWNLOAD_DIR%\%STYLE_FILE%" -C "%DESTINATION_DIR%\profile"
:: starting with version 8.6.2 the directory vanished (great!)
tar -x -f "%DOWNLOAD_DIR%\%STYLE_FILE%" -C "%DESTINATION_DIR%\profile"
