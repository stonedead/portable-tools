# Firefox

## Introduction

Compared to Chrome Firefox respects your privacy. Still, there are annoying things that can be eliminated.

official link to "mute" firefox:
https://support.mozilla.org/en-US/kb/how-stop-firefox-making-automatic-connections

more sources:
https://github.com/ghacksuserjs/ghacks-user.js
https://github.com/pyllyukko/user.js
https://ffprofile.com/

## Settings

A long list of annoyances have been removed. see `prefs.js` for full details.
* Telemetry
* Pocket
* Hello
* Shield
* Newtab/Tiles/Activity Stream
* Know you rights
* Captive detect
* Onboarding
* Oneoffserach
* Cliqz
* etc.

## Test

Test Steps:
* Start Browser with option `-jsconsole` and wait a few minutes.
* Check that only version check requests are sent (e.g. "get" https://aus5.mozilla.org/update/...)
* Check that open "new tab" sends no requests.
* Check that typing in omnibox sends no requests.

An alternative test variant could capture package (e.g. nirsoft smartsniff) instead of using console.

Console sometimes omits updates, clicking "net" twice sometimes exposes extra logs.


## Status

Following settings can not be fixed with `prefs.js`
* Search engine setup (older versions had "readable" file `search-metadata.json`)
* Extensions (chrome can "load" extensions with command line option. Firefox can not.)


## Alternative Enterprise Settings

With Version 60ESR enterprise settings are available (`app/distribution/policies.json`).
They can fix search engine setup and extensions (but does not support relative paths).

Use `download_esr.bat` and `install_esr.bat` to alternative ESR edition.

Using patched extension should be possible with ESR (`xpinstall.signatures.required=false`), need to test this.