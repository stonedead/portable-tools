setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

if not "%1" == "" set UBLOCK_VERSION=%1

set FILE=uBlock0_%UBLOCK_VERSION%.chromium.zip
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exists: %FILE%
  exit /B 0
)

set BASE_URL=https://github.com/gorhill/uBlock/releases/download
:: https://github.com/gorhill/uBlock/releases/download/1.17.4/uBlock0.chromium.zip
:: "great" to change this ...
:: https://github.com/gorhill/uBlock/releases/download/1.18.2/uBlock0_1.18.2.chromium.zip
set URL=%BASE_URL%/%UBLOCK_VERSION%/%FILE%
%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"
