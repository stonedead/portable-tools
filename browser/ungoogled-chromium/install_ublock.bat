setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=%DOWNLOAD_DIR%\uBlock0_%UBLOCK_VERSION%.chromium.zip

if not exist "%FILE%" call %~dp0download_ublock.bat

set DESTINATION_DIR=%BROWSER_BASE_DIR%\%UNGOOGLED_CHROMIUM_PATH%

%BIN%\7za.exe x "%FILE%" -o"%DESTINATION_DIR%"

:: add to switch --load-extension=
:: using %% to avoid "variable evaluation"
copy %DESTINATION_DIR%\ugc.bat %DESTINATION_DIR%\ugc.bat.tmp
%BIN%\AutoHotkeyU32c.exe %BIN%\replace.ahk %DESTINATION_DIR%\ugc.bat.tmp %DESTINATION_DIR%\ugc.bat --load-extension= --load-extension=%%~dp0uBlock0.chromium,
del %DESTINATION_DIR%\ugc.bat.tmp
