@echo off
setlocal

:: destination directories
set APP=%~dp0app
set PROFILE=%~dp0profile

:::: Command Line Options (disable with :: prefix) ::::
set ARGS=--user-data-dir=%PROFILE%

set ARGS=%ARGS% --lang=en-GB

set ARGS=%ARGS% --homepage=about:blank

:: disable proxy detection
set ARGS=%ARGS% --no-proxy-server

:: no "questions"
set ARGS=%ARGS% --no-default-browser-check

:: open blank page
set ARGS=%ARGS% about:blank

:: disable disk cache
set ARGS=%ARGS% --disk-cache-size=1 --media-cache-size=1
:: disable gpu cache
set ARGS=%ARGS% --disable-gpu-program-cache
set ARGS=%ARGS% --disable-gpu-shader-disk-cache

:: create log file...
:: set ARGS=%ARGS% --log-net-log=%~dp0net.log

:: no quic
set ARGS=%ARGS% --disable-quic

:: activate features
set ARGS=%ARGS% --flag-switches-begin --custom-ntp=about:blank --flag-switches-end

:: load extensions from filesystem
:: uMatrix or uBlock0 will be added with script on demand
set ARGS=%ARGS% --load-extension=

:: avoid side effects
reg delete HKCU\Software\Chromium /f

start %APP%\chrome.exe %ARGS%
