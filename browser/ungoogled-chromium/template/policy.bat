:: this requires admin shell (portable like a POS)

:: set up policies, see chrome://policy/ > "show policies with no value set"

:: start with clean policies
reg delete HKCU\SOFTWARE\Policies\Chromium /f

:: disable pointless UDP multicast (SSDP), EnableMediaRouter=false
reg add "HKCU\Software\Policies\Chromium" /v EnableMediaRouter /t REG_DWORD /d 0 /f

:: check result
reg query HKCU\SOFTWARE\Policies\Chromium
