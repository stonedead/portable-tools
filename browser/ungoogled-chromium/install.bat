setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set DESTINATION_DIR=%BROWSER_BASE_DIR%\%UNGOOGLED_CHROMIUM_PATH%

:: rmdir /s /q %DESTINATION_DIR%

if not "%4" == "" set DESTINATION_DIR=%4

if not "%1" == "" set CHROMIUM_VERSION=%1
if not "%2" == "" set CHROMIUM_REVISION=%2
if not "%3" == "" set CHROMIUM_ARCH=%3

set FILE=ungoogled-chromium-%UNGOOGLED_CHROMIUM_VERSION%-%UNGOOGLED_CHROMIUM_BUILD%_Win64.7z

if not exist "%DOWNLOAD_DIR%\%FILE%" call %~dp0download.bat

:: copy template files to destination
xcopy %~dp0template %DESTINATION_DIR% /Q /E /I

%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o"%DESTINATION_DIR%"
:: ren "%DESTINATION_DIR%\ungoogled-chromium-%UNGOOGLED_CHROMIUM_VERSION%-Win64" app
:: it is "great" that the directory in the zip file changes frequently
ren "%DESTINATION_DIR%\ungoogled-chromium-%UNGOOGLED_CHROMIUM_VERSION%-%UNGOOGLED_CHROMIUM_BUILD%_Win64" app
