setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


:: e.g. https://github.com/macchrome/winchrome/releases/download/v71.0.3578.98-r599034-Win64/ungoogled-chromium-71.0.3578.98-1_windows.7z
::      https://github.com/macchrome/winchrome/releases/download/v85.0.4183.83-r782793-Win64/ungoogled-chromium-85.0.4183.83-2_Win64.7z
:: QUIRX since 106
::      https://github.com/macchrome/winchrome/releases/download/v106.5249.091-M106.0.5249.91-r1036826-Win64/ungoogled-chromium-106.0.5249.91-1_Win64.7z

set FILE=ungoogled-chromium-%UNGOOGLED_CHROMIUM_VERSION%-%UNGOOGLED_CHROMIUM_BUILD%_Win64.7z
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)
set BASE_URL=https://github.com/macchrome/winchrome/releases/download

set URL=%BASE_URL%/%UNGOOGLED_CHROMIUM_QUIRX%-r%UNGOOGLED_CHROMIUM_REVISION%-Win64/%FILE%
%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"
