# Ungoogled Chromium


## Introduction

See Chromium project for according issues.

Ungoogled Chromium resolves most Chrome issues (like "massive" home calling).


### Source and Build

Source Code: https://github.com/Eloston/ungoogled-chromium

Ungoogled Chromium builds are listed here (label "ungoogled"): https://chromium.woolyss.com/

Binaries are uploaded here: https://github.com/macchrome/winchrome/


## Settings

Ungoogled Chromium needs no specific switches to "mute" home calling.

## Extensions

Extensions can be "loaded" on startup (`--load-extension`), this improves portable setup.

Current example adds "Blank New Tab Page" (as a copy via template).

Further plugins can be added on demand (scripts to download and install exist. They will modify `ugc.bat` to load extension).

### uMatrix

`install_umatrix.bat` will add uMatrix.
It patches `httpsb.js` to enable pre-configured matrix rules. see `µm.pMatrix.setCell` in code.


### uBlock Origin

`install_ublock.bat` will add uBlock Origin. 

## Test

Test procedure:
* Start Browser with option `--log-net-log=%~dp0net.log`
* Rename `net.log`
* Open `chrome://net-internals/#import`
* "drop" renamed `net.log`
* switch to events tab to review activity (check for `URL_REQUEST`).

An alternative test variant could capture package (e.g. nirsoft smartsniff) instead of `net.log`.

Note: Starting with Chromium 70  `net-internals` cannot import and show net log files.

## Status

Current minor issues:
* Bookmark bar is enabled and shows link "Import bookmarks now..."
* The extension "Blank New Tab Page", needs once a confirmation ("keep changes")