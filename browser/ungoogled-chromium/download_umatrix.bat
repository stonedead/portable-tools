setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

if not "%1" == "" set UMATRIX_VERSION=%1

set FILE=uMatrix_%UMATRIX_VERSION%.chromium.zip
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exists: %FILE%
  exit /B 0
)

set BASE_URL=https://github.com/gorhill/uMatrix/releases/download
:: https://github.com/gorhill/uMatrix/releases/download/1.3.10/uMatrix.chromium.zip
set URL=%BASE_URL%/%UMATRIX_VERSION%/%FILE%
%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"
