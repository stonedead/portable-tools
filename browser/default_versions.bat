:: product versions defaults

:: https://www.mozilla.org/en-US/firefox/releases/
set FIREFOX_VERSION=135.0.1
:: the 115 ESR
set FIREFOX_ESR_VERSION=115.20.0
:: the 102 ESR, support ended 2023-09
:: set FIREFOX_ESR_VERSION=102.15.1
:: the 91 ESR, support ended 2022-09
:: set FIREFOX_ESR_VERSION=91.13.0
:: the 78 ESR, support ended 2021-10
:: set FIREFOX_ESR_VERSION=78.15.0
:: the 68 ESR, support ended 2020-09
:: set FIREFOX_ESR_VERSION=68.12.0
:: the 60 ESR, support ended 2019-10
:: set FIREFOX_ESR_VERSION=60.9.0
:: the 52 ESR supported "old" plugins, support ended 2018-08
:: set FIREFOX_ESR_VERSION=52.9.0

:: architecture win32 vs win64
set FIREFOX_ARCH=win64
if %PROCESSOR_ARCHITECTURE%==ARM64 set FIREFOX_ARCH=win64-aarch64

:: remove crappy proton style, revert to photon https://github.com/black7375/Firefox-UI-Fix
set FIREFOX_STYLE_VERSION=v8.6.5
set FIREFOX_STYLE_URL=https://github.com/black7375/Firefox-UI-Fix/releases/download/%FIREFOX_STYLE_VERSION%
set FIREFOX_STYLE_FILE=Lepton-Photon-Style
:: "v116 or Lower"
set FIREFOX_STYLE_ESR_FILE=ESR-Lepton-Photon-Style

:: https://www.thunderbird.net/en-US/thunderbird/releases/
set THUNDERBIRD_VERSION=128.7.1
:: set THUNDERBIRD_VERSION=115.15.0
:: set THUNDERBIRD_VERSION=115.15.0
:: set THUNDERBIRD_VERSION=102.15.1
:: set THUNDERBIRD_VERSION=91.13.0
:: set THUNDERBIRD_VERSION=78.14.0

set THUNDERBIRD_ARCH=win32
if %PROCESSOR_ARCHITECTURE%==ARM64 set THUNDERBIRD_ARCH=win64-aarch64

:: https://github.com/gorhill/uMatrix/releases
:: https://addons.mozilla.org/en-US/firefox/addon/umatrix/
set UMATRIX_VERSION=1.4.4

:: https://github.com/gorhill/uBlock/releases
:: https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/
set UBLOCK_VERSION=1.62.0

:: addon download url (signed version)
:: changed back to mozilla with 1.31.0? github download is "forbidden?"
:: now using fixed url https://addons.mozilla.org/firefox/downloads/latest/ublock-origin/latest.xpi
:: set UBLOCK_FIREFOX_URL=https://addons.mozilla.org/firefox/downloads/file/4359936/ublock_origin-1.60.0.xpi
:: set UBLOCK_FIREFOX_URL=https://github.com/gorhill/uBlock/releases/download/1.30.10/uBlock0_1.30.10.firefox.signed.xpi


:: no proper official source for chromium for windows
:: download variants:
:: https://chromium.woolyss.com/

:: chrome 67,68,69,70 are "frozen" on win7/win10 with current setup
:: critical switch is: --disable-background-tasks (without switch home calling happens)

:: ON HOLD ::
:: set CHROMIUM_VERSION=71.0.3578.80
:: set CHROMIUM_REVISION=599034
:: architecture win32 vs win64
:: set CHROMIUM_ARCH=win64

:: only win64
set UNGOOGLED_CHROMIUM_VERSION=133.0.6943.130
set UNGOOGLED_CHROMIUM_QUIRX=v133.6943.130-M133.0.6943.130
set UNGOOGLED_CHROMIUM_REVISION=1402768
set UNGOOGLED_CHROMIUM_BUILD=1

:: https://vivaldi.com/blog/ version is shown on right
:: https://downloads.vivaldi.com/stable/Vivaldi.7.0.3495.27.arm64.exe
:: https://downloads.vivaldi.com/stable/Vivaldi.7.0.3495.27.x64.exe
set VIVALDI_VERSION=7.1.3570.54
set VIVALDI_ARCH=x64
if %PROCESSOR_ARCHITECTURE%==ARM64 set VIVALDI_ARCH=arm64