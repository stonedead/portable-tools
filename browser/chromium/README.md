# Chromium

**_THIS PRODUCT IS ON HOLD_**

New approach is ungoogled-chromium, see according directory.

## Introduction

Chrome and Chromium have multiple issues.

### Chromium vs Chrome

* Chrome is not open source.
* Chrome forces you to accept shady google terms.
* Chrome does "massive" home calling, it is hard to track what data is transferred "home".

check this link for home calling details:
https://digitalcontentnext.org/blog/2018/08/21/google-data-collection-research/

### Chromium

Chromium is open source, but google gives you no proper build (only snapshots).
See https://chromium.woolyss.com/ for all the gory details.

Chromium hardly resolves Chrome issues (like "massive" home calling).

The current setup uses henrypp builds.
https://github.com/henrypp/chromium/releases/

Ungoogled chrome might be a good alternative that requires less fixing.
https://github.com/Eloston/ungoogled-chromium


## Settings

It takes many magical switches to "mute" Chromium home calling. Some hints are here:
http://peter.sh/experiments/chromium-command-line-switches/

Some hints can only be found in source code (e.g. options for `--disable-features`).

## Extensions

Extensions can be "loaded" on startup (`--load-extension`), this improves portable setup.

Current example adds "Blank New Tab Page" (as a copy via template), and downloads uMatrix from github.

install_umatrix.bat patches `httpsb.js` to enable pre-configured matrix rules.
see `µm.pMatrix.setCell` in code.


## Test

Test procedure:
* Start Browser with option `--log-net-log=%~dp0net.log`
* Rename `net.log`
* Open `chrome://net-internals/#import`
* "drop" renamed `net.log`
* switch to events tab to review activity (check for `URL_REQUEST`).

An alternative test variant could capture package (e.g. nirsoft smartsniff) instead of `net.log`.

## Status

**_ON HOLD!_**

This is getting tedious:
* The henrypp build has no "nosync" edition for chrome 68
* Starting with chrome 67 the switch `--disable-background-tasks` causes a "freeze". without this switch home calling happens. 
* Conclusion: Switching (soon) to *ungoogled-chromium*

Chrome downloads (sometimes?) pnacl things via BITS (background intelligent transfer service).

Some settings can not (yet) be fixed with command line options.

suggested further manual setup: disable in settings-advanced.
* Use a web service to help resolve navigation errors
* Protect you and your device from dangerous sites
* Possibly more..
