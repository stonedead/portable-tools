setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=chromium-sync-%CHROMIUM_VERSION%-%CHROMIUM_ARCH%.zip
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)
set BASE_URL=https://github.com/henrypp/chromium/releases/download
:: https://github.com/henrypp/chromium/releases/download/v67.0.3396.87-r550428-win64/chromium-nosync.zip
:: last "nosync" version by henrypp is 67
set URL=%BASE_URL%/v%CHROMIUM_VERSION%-r%CHROMIUM_REVISION%-%CHROMIUM_ARCH%/chromium-sync.zip
%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"
:: rename with version
ren %DOWNLOAD_DIR%\chromium-sync.zip %FILE%
