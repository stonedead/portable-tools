setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=%DOWNLOAD_DIR%\uMatrix.chromium-%UMATRIX_VERSION%.zip

set DESTINATION_DIR=%BROWSER_BASE_DIR%\%CHROMIUM_PATH%

%BIN%\7za.exe x "%FILE%" -o"%DESTINATION_DIR%"

:: create backup
copy "%DESTINATION_DIR%\uMatrix.chromium\js\httpsb.js" "%DESTINATION_DIR%\uMatrix.chromium\js\httpsb.js.orig"
:: patch javascript with initial rules...
copy  /Y "%~dp0template_umatrix\httpsb.js" "%DESTINATION_DIR%\uMatrix.chromium\js"