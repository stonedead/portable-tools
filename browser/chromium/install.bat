setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set DESTINATION_DIR=%BROWSER_BASE_DIR%\%CHROMIUM_PATH%

rmdir /s /q %DESTINATION_DIR%

if not "%4" == "" set DESTINATION_DIR=%4

if not "%1" == "" set CHROMIUM_VERSION=%1
if not "%2" == "" set CHROMIUM_REVISION=%2
if not "%3" == "" set CHROMIUM_ARCH=%3

set FILE=chromium-sync-%CHROMIUM_VERSION%-%CHROMIUM_ARCH%.zip

:: copy template files to destination
xcopy %~dp0template %DESTINATION_DIR% /Q /E /I

%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o"%DESTINATION_DIR%"
ren "%DESTINATION_DIR%\chrome-win32" app
