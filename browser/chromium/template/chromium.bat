@echo off
setlocal

:: destination directories
set APP=%~dp0app
set PROFILE=%~dp0profile

:::: Command Line Options (disable with :: prefix) ::::
set ARGS=--user-data-dir=%PROFILE%

set ARGS=%ARGS% --lang=en-GB

set ARGS=%ARGS% --homepage=about:blank

:: disable proxy detection
set ARGS=%ARGS% --no-proxy-server


:: disable disk cache
set ARGS=%ARGS% --disk-cache-size=1 --media-cache-size=1
:: disable gpu cache
set ARGS=%ARGS% --disable-gpu-program-cache
set ARGS=%ARGS% --disable-gpu-shader-disk-cache

:: disable "safe browsing" crap, avoid download
set ARGS=%ARGS% --disable-client-side-phishing-detection
set ARGS=%ARGS% --safebrowsing-disable-auto-update

:: disable background activity
:: this breaks chromium 67+ (without switch "home calling" happens)
:: set ARGS=%ARGS% --disable-background-tasks
set ARGS=%ARGS% --disable-background-networking --disable-component-extensions-with-background-pages

:: create log file...
:: set ARGS=%ARGS% --log-net-log=%~dp0net.log

:: no "questions". still displaying "welcome" shit
set ARGS=%ARGS% --no-default-browser-check --no-first-run
:: open blank page, this avoids "welcome" shit
set ARGS=%ARGS% about:blank

:: testing?
set ARGS=%ARGS% --disable-default-apps
set ARGS=%ARGS% --disable-infobars

:: wtf "hyperlink auditing pings" 
set ARGS=%ARGS% --no-pings

:: NTP new tab page
set ARGS=%ARGS% --disable-icon-ntp --disable-ntp-most-likely-favicons-from-server --disable-ntp-popular-sites --force-local-ntp
:: features that do nothing when disabled DisplaySuggestionsServiceTiles,UsePopularSitesSuggestions,NTPPopularSitesBakedInContent,NTPMostLikelyFaviconsFromServer
:: finally elimited with extension...

:: no home calling
set ARGS=%ARGS% --disable-sync --disable-sync-app-list 

:: no quic
set ARGS=%ARGS% --disable-quic

:: to be sure..
set ARGS=%ARGS% --disable-cloud-import

:: no home calling: this avoids two google.com calls
:: NOTE this switch destroys chrome 49 on XP (100% CPU burner)
set ARGS=%ARGS% --skip-gpu-data-loading

:: disable translate ranker shit and other features
:: test WebAssembly
set ARGS=%ARGS% --disable-features=TranslateRankerQuery,TranslateRankerEnforcement,NetworkTimeServiceQuerying,DisableMetrics

:: this does nothing?
set ARGS=%ARGS% --disable-user-metrics

:: add extensions from filesystem
:: 1) uMatrix
:: 2) blank ntp (cannot configure this shit), it still shows "warning"?
set ARGS=%ARGS% --load-extension=%~dp0uMatrix.chromium,%~dp0blank_ntp

start %APP%\chrome.exe %ARGS%
