:: install paths

set BROWSER_BASE_DIR=%INSTALL_BASE_DIR%\browser
set FIREFOX_PATH=firefox
set THUNDERBIRD_PATH=thunderbird
set CHROMIUM_PATH=chromium
set UNGOOGLED_CHROMIUM_PATH=ungoogled-chromium
set VIVALDI_PATH=vivaldi