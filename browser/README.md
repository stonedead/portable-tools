# Portable Web Browsers

## Introduction

Browsers are optimized for:
* Privacy (NO home calling, no unwanted features, no unsolicited actions)
* Avoid Annoyances (no "welcome" messages, no obstructions, just "shut t.f. up")

Both properties lead to the "stonedead" edition.

Firefox and Ungoogled Chromium are available. see according readme files.

Thunderbird is not a browser but works similar to Firefox.
