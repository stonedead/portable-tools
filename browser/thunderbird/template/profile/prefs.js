/*
  based on https://github.com/HorlogeSkynet/thunderbird-user.js/blob/master/user.js
  see also https://github.com/mozilla/releases-comm-central/blob/master/mail/app/profile/all-thunderbird.js
*/

/* annoyances */

user_pref("mailnews.start_page.enabled", false);
user_pref("mail.shell.checkDefaultClient", false);

user_pref("browser.aboutConfig.showWarning", false);
// don't show "know your rights"
user_pref("mail.rights.version", 1);

// user_pref("mailnews.auto_config.guess.enabled", false);
user_pref("mailnews.auto_config_url", "");
user_pref("mailnews.auto_config.addons_url", "");

/* updates */

// https://services.addons.thunderbird.net/api/v3/addons/search/
user_pref("extensions.systemAddon.update.enabled", false);
user_pref("extensions.update.enabled", false);
// don't update search engines?
user_pref("browser.search.update", false);

/* home calling (telemetry, settings) */
// https://github.com/mozilla/releases-comm-central/blob/master/mail/components/telemetry/README.md
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.enabled", false);
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("toolkit.telemetry.server", "data:,");


user_pref("datareporting.policy.dataSubmissionEnabled", false);
/* [SETTING] Privacy & Security > Thunderbird Data Collection and Use > Allow Thunderbird to send technical... */
user_pref("datareporting.healthreport.uploadEnabled", false);

// disable "remote settings"
user_pref("services.settings.server", "");

// disable captive crap.
user_pref("network.captive-portal-service.enabled", false);
// remove more "detection", does (currently) not exist in TB
// user_pref("network.connectivity-service.enabled", false);

// disable disk cache
user_pref("browser.cache.disk.enable", false);

/* view */

user_pref("javascript.enabled", false);

user_pref("ui.prefersReducedMotion", 0);

/* geolocation */
user_pref("geo.enabled", false);
// user_pref("geo.provider.network.url", "");
user_pref("geo.provider.ms-windows-location", false);
user_pref("browser.region.network.url", "");
user_pref("browser.region.update.enabled", false);
// user_pref("browser.search.geoip.url", "");

/* some useful user settings and tweaks */

// compose
user_pref("mail.html_compose", false);
user_pref("mail.identity.default.compose_html", false);
user_pref("mailnews.sendformat.auto_downgrade", true);
user_pref("mailnews.wraplength", 0);
// enable tray
user_pref("mail.minimizeToTray", true);
// language
user_pref("intl.accept_languages", "en-US, en");
// formats
// avoid "variable" date format http://kb.mozillazine.org/Date_display_format. 2=short.
user_pref("mail.ui.display.dateformat.today", 2);
// detail control date/time format: https://support.mozilla.org/en-US/kb/customize-date-time-formats-thunderbird
// user_pref("intl.date_time.pattern_override.date_short", "dd.MM.yyyy");
// user_pref("intl.date_time.pattern_override.time_short", "HH:mm");

// do not use "reduced motion" from OS (ugly hourglass)
user_pref("ui.prefersReducedMotion", 0);

/* security related */

user_pref("security.OCSP.enabled", 0);

// some console settings
user_pref("devtools.webconsole.timestampMessages", true);
user_pref("devtools.browserconsole.contentMessages", true);
user_pref("devtools.browserconsole.filter.net", true);
user_pref("devtools.browserconsole.filter.netxhr", true);
// don't lecture me
user_pref("devtools.webconsole.input.editorOnboarding", false);

// disable bits
user_pref("app.update.BITS.enabled", false);

// disable "end of year" donation web popup (TB102)
user_pref("app.donation.eoy.version.viewed", 666);

// disable "beta appeal" web popup (TB102)
pref("app.beta_appeal.version.viewed", 666);

// cannot disable "whats new" web popup in case of version update
// user_pref("mailnews.start_page_override.mstone", "999.0.0");
// user_pref("mailnews.start_page.override_url", "");