@echo off
setlocal
:: destination directories
set APP=%~dp0app
set PROFILE=%~dp0profile

:::: Command Line Options (disable with :: prefix) ::::

:: select profile directory
set ARGS=-profile %PROFILE%

:: enable multiple instances (avoiding socket lock)
set ARGS=%ARGS% -no-remote 

set ARGS=%ARGS% --disable-startupcache

:: start with console (allows to check remote/home calls during startup)
:: set ARGS=%ARGS% -jsconsole

:: without this magical switch setting "services.settings.server" will be ignored
set XPCSHELL_TEST_PROFILE_DIR=fuck-mozilla-settings
set MOZ_REMOTE_SETTINGS_DEVTOOLS=1

start %APP%\thunderbird.exe %ARGS%