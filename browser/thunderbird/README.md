# Thunderbird

## Introduction

Thunderbird works similar to Firefox.

## Settings

Following annoyances have been removed. see `prefs.js` for full details.
* no default client question
* no "know your rights"
* don't confirm calendar usage
* 

## Test

Test Steps:
* Start client with option `-jsconsole` and wait a few minutes.
* Check that only version check requests are sent

An alternative test variant could capture package (e.g. nirsoft smartsniff) instead of using console.

Console sometimes omits updates, clicking "net" twice sometimes exposes extra logs.

## Status

initial draft version
