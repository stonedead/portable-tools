setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


set FILE=Thunderbird Setup %THUNDERBIRD_VERSION%.exe
set FILE_ARCH=Thunderbird Setup %THUNDERBIRD_VERSION%_%THUNDERBIRD_ARCH%.exe

if exist "%DOWNLOAD_DIR%\%FILE_ARCH%" (
  echo file exits: %FILE_ARCH%
  exit /B 0
)

:: url encoded file name
set FILE_URL=Thunderbird%%20Setup%%20%THUNDERBIRD_VERSION%.exe

set BASE_URL=https://download-installer.cdn.mozilla.net/pub/thunderbird/releases
:: TODO language
set URL=%BASE_URL%/%THUNDERBIRD_VERSION%/%THUNDERBIRD_ARCH%/en-US/%FILE_URL%
%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"

ren "%DOWNLOAD_DIR%\%FILE%" "%FILE_ARCH%"