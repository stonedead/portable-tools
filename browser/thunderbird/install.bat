setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


set DESTINATION_DIR=%BROWSER_BASE_DIR%\%THUNDERBIRD_PATH%

:: rmdir /s /q %DESTINATION_DIR%

set FILE=Thunderbird Setup %THUNDERBIRD_VERSION%_%THUNDERBIRD_ARCH%.exe

if not exist "%DOWNLOAD_DIR%\%FILE%" call %~dp0download.bat

:: copy template files to destination
xcopy %~dp0template %DESTINATION_DIR% /Q /E /I

:: extract "core" from installer to %DESTINATION_DIR%\app
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o"%DESTINATION_DIR%"
del "%DESTINATION_DIR%\setup.exe"
ren "%DESTINATION_DIR%\core" app