# Portable Installation Tools

This tools can be used to create portable products installations. 
They target "advanced" users - you have been warned.


Products are optimized for:
* Avoiding setup Tasks (losing time by clicking through configuration dialogs)
* Efficiency (low disk usage, quick start, etc.)

## General File Structure

`setup`: holds binaries required for setup (wget, 7z, etc).

`download`: will hold downloaded original packages. works as cache. override with `user_paths.bat`.

`test`: will be used by default to install portable tools. override with `user_paths.bat`.

`groupX`: group specific directory (e.g. browser)


## Group Specific File Structure

`groupX\README.md`: group specific readme file

`groupX\productX`: product specific directory will hold scripts and files.

`groupX\default_versions.bat`: holds current versions and other defaults

`groupX\user_versions.bat`: user specific "override" file

`groupX\default_paths.bat`: holds current default install paths

`groupX\user_paths.bat`: user specific "override" paths


## Product specific File Structure

`groupX\productX\README.md`: product specific readme file

`groupX\productX\download.bat` download binary package

`groupX\productX\install.bat` create customized installation (will call `download.bat` if required)

`groupX\productX\template` installation template files

`groupX\productX\README.md` detail notes and links

## Further notes

Depending on the product not all configuration can be done by templates.

Version migration is out of scope, this tools support only clean setup.

Feel free to adjust templates, they represent the preferred flavor of the author and should be easy to change.

## Optional installation

Windows 10 comes with curl and tar since 1809.

`curl -L https://gitlab.com/stonedead/portable-tools/-/archive/master/portable-tools-master.tar.gz | tar xz --strip-components=1 -f -`