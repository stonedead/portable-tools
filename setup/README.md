# Setup Binaries

The binaries in this folder are required for download and installation scripts.

For each binary the source URL can be found in the according text file.

The files were selectively extracted and upx-ed, no installation process was involved.

The source URL will clarify according licenses.

`replace_env.ahk` is an autohotkey script that replaces tokens in a file based on environment variables.

It uses patched `AutoHotkeyU32c.exe` see `autohotkey.txt`.
