; this script searches and replaces text in files, currently no multi line support for search or replace text
; TODO: maybe support multiple search and replace text
; replace.ahk infile outfile search_text replace_text
; NOTE: use double quotes if test contains spaces
; NOTE: only windows line breaks ("\r\n") are supported

; Note: this script is used with "command line" patched autohotkey
; see AutoHokey.txt for details

#NoTrayIcon

; for error messages
StdOut := FileOpen("*", "w `n")

argCount = %0%

if (argCount != 4) {
    StdOut.WriteLine("Usage: replace.ahk input output searchTerm replaceTerm")
    StdOut.WriteLine("   Note: output * writes to stdout")
    ExitApp -1
}

InFileName = %1%
OutFileName = %2%
SearchText = %3%
ReplaceText = %4%

if (!FileExist(InFileName)) {
   StdOut.WriteLine("Input not found: " InFileName)
   ExitApp -1
}

OutFile := FileOpen(OutFileName, "w `n")

Loop, Read, %InFileName% 
{    
    replaced := StrReplace(A_LoopReadLine, SearchText, ReplaceText)
    OutFile.WriteLine(replaced)
}