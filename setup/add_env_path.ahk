; add the directory with batch files (%DEV_BASE_DIR%) to PATH via HKCU\Environment\Path

#NoTrayIcon

StdOut := FileOpen("*", "w `n")

EnvGet BaseDir, DEV_BASE_DIR
if (BaseDir == "") {
    StdOut.WriteLine("ERROR: Missing DEV_BASE_DIR environment.")
    ExitApp -1
}

; read current value
RegRead RegPath, HKCU\Environment, Path
; StdOut.WriteLine("current path=" RegPath)

; Note: this is not "100%" precise (without parsing delimiter ";"). 
InPath := InStr(RegPath, BaseDir)

if (InPath > 0) {
    StdOut.WriteLine("Directory already in path.")
    ExitApp 0
}

NewPath := BaseDir ";" RegPath

StdOut.WriteLine("Setting new path=" NewPath)

RegWrite REG_SZ, HKCU\Environment, Path, %NewPath%

SendMessage, 0x1A,0,"Environment",, ahk_id 0xFFFF ; 0x1A is WM_SETTINGCHANGE, 0xFFFF is broadcast
