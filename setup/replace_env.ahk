; this script replaces tokens in brackets with environment variables
; ${MY_VAR} is replaced with value of %MY_VAR%
; if environment variable is not set tags will not be replaced

; Note: this script is used with "command line" patched autohotkey
; see AutoHokey.txt for details

#NoTrayIcon

; for error messages
StdOut := FileOpen("*", "w `n")

argCount = %0%

if (argCount != 2) {
    StdOut.WriteLine("Usage: replace.ahk input output")
    StdOut.WriteLine("   Note: output * writes to stdout")
    ExitApp -1
}

InFileName = %1%
OutFileName = %2%

InFile := FileOpen(InFileName, "r")
if (A_LastError > 0) {
    StdOut.WriteLine("File Not Found " InFileName)
    ExitApp -1
}

OutFile := FileOpen(OutFileName, "w")
; this does not work, creates false "errors"
;if (A_LastError > 0) {
;    StdOut.WriteLine("Could not write to " OutFileName)
;    ExitApp -1
;}

Loop {
    ; could improve this with "buffered" reader?
    c := InFile.read(1)  ; read one character
    if (c == "") { ; no more chars (i.e. EOF)
        ExitApp -1 ; this seems to flush buffers
    }
    if (c == "$") {  ; might be tag, check next char
        c := InFile.read(1) ; Note: case c == "" works
        if (c == "{") { ; start of tag
            tag := ""
            Loop { ; find end of tag
                c := InFile.read(1)
                if (c == "") { 
                    StdOut.WriteLine("Missing closing bracket")
                    ExitApp -1
                }
                if (c == "}") {
                    if (tag == "") {
                        OutFile.write("${}")  ; leave unchanged
                        break
                    }
                    EnvGet value, %tag%
                    if (value == "") {  ; not set
                        OutFile.write("${" tag "}")  ; leave unchanged
                        break
                    }
                    else {
                        OutFile.write(value)  ; finally replace this with env
                        break
                    }
                }
                else {
                    tag .= c
                }
            }
        }
        else { ; $ but not ${
            OutFile.write("$" c) 
        }
    }
    else { ; no $
        OutFile.write(c)
    }
}
