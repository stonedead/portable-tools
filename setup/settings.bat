:: general settings and paths

@echo off

:: base bath of tools
set TOOL_PATH=%~dp0..

:: path of setup binaries
set BIN=%~dp0
:: strip trailing \
set BIN=%BIN:~0,-1%

if "%INSTALL_BASE_DIR%"=="" set INSTALL_BASE_DIR=%TOOL_PATH%\test

if "%DOWNLOAD_DIR%"=="" set DOWNLOAD_DIR=%TOOL_PATH%\download

