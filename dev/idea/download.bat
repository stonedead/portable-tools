setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

:: https://www.jetbrains.com/idea/download/
:: https://download.jetbrains.com/idea/ideaIC-2018.1.5.win.zip
:: https://download.jetbrains.com/idea/ideaIC-2018.1.6.win.zip.sha256
set FILE=ideaIC-%IDEA_VERSION%.win.zip
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)
set BASE_URL=https://download.jetbrains.com/idea
set URL=%BASE_URL%/%FILE%
%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"
%BIN%\wget.exe %URL%.sha256 -P "%DOWNLOAD_DIR%"