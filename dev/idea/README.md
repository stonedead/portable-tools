# Portable Intellij IDEA

## Introduction

Since the release of the community edition as open source project there is no other useful choice for a Java IDE.

download url: https://www.jetbrains.com/idea/download/

Script downloads Community edition, Ultimate should work but is not tested.


## Portability changes

Idea can be made portable, some details are tricky.

`bin/idea_exe64.bat` sets `JAVA_HOME`, `GIT_SSH` and runs `idea64.exe` (this was more stable than using the batch file)

`bin/idea.properties` use path relative to executable

```
idea.config.path=${idea.home.path}/../../userdata/.IdeaIC/config
idea.system.path=${idea.home.path}/../../userdata/.IdeaIC/system
```

Relative binary paths:

`.IdeaIC201801/config/options/jdk.table.xml` will use `$APPLICATION_HOME_DIR$/../../bin/jdk1.8.0_171_x64`

`.IdeaIC201801/config/options/git.xml` uses `$APPLICATION_HOME_DIR$/../../bin/PortableGit-2.18.0/bin/git.exe`

Note that starting idea (2017.3 and later) will "break" `jdk.table.xml` as it writes back file with absolute paths (non portable).

## Predefined settings

All settings are stored in `.IdeaIC/config` as xml files. 
The installation script will copy templates from `template_config`.

The files were created by making changes with settings dialog and selecting the minimal set of files and xml entries.

See `template_config` for all details.

Note: Most files can be deleted and will bring you back to Idea defaults.

## Update version of products used by idea

`update_dep.bat` will update all files that reference versions of jdk, git and putty

Note that idea must be stopped before applying this update.

