setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

:: update files that reference JDK or GIT versions

set DESTINATION_DIR=%IDE_BASE_DIR%\idea-%IDEA_VERSION%

if not exist %DESTINATION_DIR%\bin\idea_exe64.bat (
   ECHO Idea is not installed, cannot upgrade dependant products
   exit /B 0
)

:: replace product versions in some options
%BIN%\AutoHotkeyU32c.exe %BIN%\replace_env.ahk %~dp0template_config\options\git.xml %DEV_USER_DIR%\%IDEA_PROFILE%\config\options\git.xml

:: prepare JDK_HOME_PATH as this will be used 20+ times in config
set JDK_HOME_PATH=openjdk-8u%OPENJDK8_VERSION%_%OPENJDK8_ARCH%
%BIN%\AutoHotkeyU32c.exe %BIN%\replace_env.ahk %~dp0template_config\options\jdk.table.xml %DEV_USER_DIR%\%IDEA_PROFILE%\config\options\jdk.table.xml

:: replace product versions in idea_exe.bat
%BIN%\AutoHotkeyU32c.exe %BIN%\replace_env.ahk %~dp0template\bin\idea_exe64.bat %DESTINATION_DIR%\bin\idea_exe64.bat
