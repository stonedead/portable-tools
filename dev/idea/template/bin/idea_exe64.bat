:: mute some annoyance
reg add HKCU\SOFTWARE\JavaSoft\Prefs\jetbrains\privacy_policy /f /t REG_SZ /v accepted_version /d 999.999
reg add HKCU\SOFTWARE\JavaSoft\Prefs\jetbrains\privacy_policy /f /t REG_SZ /v euacommunity_accepted_version /d 999.999

set JAVA_HOME=%~dp0..\..\..\bin\${JDK17_HOME_PATH}
set GIT_SSH=%~dp0..\..\..\bin\putty-${PUTTY_VERSION}_x${PUTTY_ARCH}\plink.exe
:: enable portable .gitconfig
set HOME=%~dp0..\..\..\userdata
:: used in maven settings.xml
set M2_ROOT=%~dp0..\..\..\userdata\.m2
start %~dp0idea64.exe