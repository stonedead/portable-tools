setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=ideaIC-%IDEA_VERSION%.win.zip

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%IDE_BASE_DIR%\idea-%IDEA_VERSION%

rmdir /s /q %DESTINATION_DIR%

%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o%DESTINATION_DIR%

:: remove jbr (using corretto 17)
rmdir /s /q %DESTINATION_DIR%\jbr

:: fix awt link error ("java.lang.UnsatisfiedLinkError: ... awt.dll: Can't find dependent libraries")
del %DESTINATION_DIR%\bin\msvcp140.dll

:: remove all "bundled" (i.e. dubious closed source) plugins
:: see https://www.jetbrains.com/legal/community-bundled-plugins/
:: remove marketplace
rmdir /s /q %DESTINATION_DIR%\plugins\marketplace
:: remove "code with me" (95MB with native code)
rmdir /s /q %DESTINATION_DIR%\plugins\cwm-plugin
rmdir /s /q %DESTINATION_DIR%\plugins\cwm-plugin-projector
:: remove space (66MB)
rmdir /s /q %DESTINATION_DIR%\plugins\space
:: remove shared index
rmdir /s /q %DESTINATION_DIR%\plugins\indexing-shared

:: remove bloated android plugins (450MB)
rmdir /s /q %DESTINATION_DIR%\plugins\android
rmdir /s /q %DESTINATION_DIR%\plugins\android-gradle-dsl
rmdir /s /q %DESTINATION_DIR%\plugins\design-tools

:: remove packageSearch, does "automatic" remote calls (update checks for libs?)
rmdir /s /q %DESTINATION_DIR%\plugins\packageSearch

:: TODO edit templates with versions

:: keep original file
ren %DESTINATION_DIR%\bin\idea.properties idea.properties.orig
:: copy template files to destination
xcopy %~dp0template %DESTINATION_DIR% /Q /E /I

:: copy settings template
xcopy %~dp0template_config %DEV_USER_DIR%\%IDEA_PROFILE%\config /Q /E /I

:: replace product versions in some options (this will overwrite already copied files)
%BIN%\AutoHotkeyU32c.exe %BIN%\replace_env.ahk %~dp0template_config\options\windows\git.xml %DEV_USER_DIR%\%IDEA_PROFILE%\config\options\windows\git.xml

:: prepare JDK_HOME_PATH as this will be used 20+ times in config
set JDK_HOME_PATH=openjdk-8u%OPENJDK8_VERSION%_%OPENJDK8_ARCH%
set JDK_HOME_VERSION=8u%OPENJDK8_VERSION%
set JDK11_HOME_PATH=openjdk-%OPENJDK11_VERSION%_%OPENJDK11_ARCH%
set JDK11_HOME_VERSION=%OPENJDK11_VERSION%
set JDK17_HOME_PATH=openjdk-%OPENJDK17_VERSION%_%OPENJDK17_ARCH%
set JDK17_HOME_VERSION=%OPENJDK17_VERSION%
%BIN%\AutoHotkeyU32c.exe %BIN%\replace_env.ahk %~dp0template_config\options\jdk.table.xml %DEV_USER_DIR%\%IDEA_PROFILE%\config\options\jdk.table.xml

:: replace product versions in idea_exe.bat
%BIN%\AutoHotkeyU32c.exe %BIN%\replace_env.ahk %~dp0template\bin\idea_exe64.bat %DESTINATION_DIR%\bin\idea_exe64.bat

:: mute more annoyance
copy /b %DESTINATION_DIR%\bin\idea64.exe.vmoptions + /b %DESTINATION_DIR%\bin\idea64.exe.vmoptions.add %DESTINATION_DIR%\bin\idea64.exe.vmoptions.new
del %DESTINATION_DIR%\bin\idea64.exe.vmoptions
ren %DESTINATION_DIR%\bin\idea64.exe.vmoptions.new idea64.exe.vmoptions