setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=mariadb-%MARIADB_VERSION%-%MARIADB_ARCH%.zip

if not exist "%DOWNLOAD_DIR%\%FILE%" call %~dp0download_maria.bat

echo Installing MariaDB %MARIADB_VERSION%

:: PATH in zip file
set MYSQL_PATH=mariadb-%MARIADB_VERSION%-%MARIADB_ARCH%
set DATA=%DEV_USER_DIR%\mysql


:: extract and upx selected binaries
for %%F in (mysql.exe mysqld.exe mysqldump.exe mysqladmin.exe server.dll mariadb-install-db.exe) do (
  %BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" %MYSQL_PATH%/bin/%%F -o%BIN_BASE_DIR%
  %BIN%\upx.exe %BIN_BASE_DIR%/%MYSQL_PATH%/bin/%%F
)
:: extract locales, charsets, etc.
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" %MYSQL_PATH%/share/* -o%BIN_BASE_DIR%

:: copy template with bat scripts and ini file
xcopy /s /q /i %~dp0template %BIN_BASE_DIR%\%MYSQL_PATH%