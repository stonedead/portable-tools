# Portable MySQL

## Introduction

This mysql setup is designed as small and fast developer setup.

## Installation

`dev\mysql\install.bat`.

Mysql binaries are stripped and upx'ed for basic use cases.
The "footprint" was reduced from more than 800MB (in zip variant, not sure about msi) to 5MB.

Version 5.5 was selected as oldest supported version (5.1 also works fine). \
Version 5.6 consumes twice the space in data directory. \
Version 5.7 no longer contains default data directory in zip file, it requires `--initialize-insecure` instead that consumes extra time.

Features added to 5.6 and 5.7 did not amaze me:

https://dev.mysql.com/doc/refman/5.6/en/mysql-nutshell.html
https://dev.mysql.com/doc/refman/5.7/en/mysql-nutshell.html

For my simple tests each "upgrade" caused that binary size increased, data size increased and performance dropped.

## Configuration

Basic configuration changes data directory to `userdata\mysql\data`. \
root login without password. \
Default port 3306.

check `bin\mysql-5.x.x\my.ini` for details and tweaks.

The data directory requires initial 30MB. 
The configuration of `ibdata1` is broken, it should be 10MB but uses 18MB.
The bug was introduced with version 5.5 (5.1 was OK).


## Shell Install and Usage

`dev\shell\install_mysql.bat` will create `me.bat`. 

calling `%DEV_HOME%\me.bat` will  add the path to mysql scripts.

`mysql_start.bat` starts mysqld \
`mysql_stop.bat` stops mysqld \
`mysql_clean.bat` cleans data directory. assumes stopped mysqld. _be careful with this script_. \
`mysql.bat` \
`mysqladmin.bat` \
`mysqldump.bat`

Note: Shell installation is an option. The batch files are portable and can be called directly.

## MariaDB alternative

These scripts will create a similar setup with MariaDB.

`dev\mysql\install_maria.bat`.

`dev\shell\install_mariadb.bat` will create `me.bat`.

Note that this _replaces_ MySQL using identical scripts and data files. Mixed setup is not supported.


