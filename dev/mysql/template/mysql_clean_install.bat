@echo off

:: script assumes stopped mysqld. 
:: works for 10.5

:: will delete data and reset to original files from data.clean

rmdir /s /q %~dp0..\..\userdata\mysql
mkdir %~dp0..\..\userdata\mysql

start %~dp0bin\mariadb-install-db.exe --datadir=%~dp0..\..\userdata\mysql\data
