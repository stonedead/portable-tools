@echo off

:: script assumes stopped mysqld.
:: works for 10.3 (zip file contains data/mysql*)

:: will delete data and reset to original files from data.clean

rmdir /s /q %~dp0..\..\userdata\mysql\data
xcopy /s /q /i %~dp0..\..\userdata\mysql\data-clean %~dp0..\..\userdata\mysql\data

