setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


:: https://downloads.mariadb.org/mariadb/+releases/
:: https://downloads.mariadb.org/interstitial/mariadb-10.3.12/winx64-packages/mariadb-10.3.12-winx64.zip
:: http://downloads.mariadb.org/rest-api/mariadb/10.5.4/mariadb-10.5.4-win32.zip

set FILE=mariadb-%MARIADB_VERSION%-%MARIADB_ARCH%.zip

if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)
set BASE_URL=http://downloads.mariadb.org/rest-api/mariadb

set URL=%BASE_URL%/%MARIADB_VERSION%/%FILE%

%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"