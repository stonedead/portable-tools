setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=mysql-%MYSQL_VERSION%-%MYSQL_ARCH%.zip

if not exist "%DOWNLOAD_DIR%\%FILE%" call %~dp0download.bat

echo Installing MySQL %MYSQL_VERSION%

:: PATH in zip file
set MYSQL_PATH=mysql-%MYSQL_VERSION%-%MYSQL_ARCH%
set DATA=%DEV_USER_DIR%\mysql

:: extract and upx selected binaries
for %%F in (mysql.exe mysqld.exe mysqldump.exe mysqladmin.exe) do (
  %BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" %MYSQL_PATH%/bin/%%F -o%BIN_BASE_DIR%
  %BIN%\upx.exe %BIN_BASE_DIR%/%MYSQL_PATH%/bin/%%F
)
:: extract locales, charsets, etc.
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" %MYSQL_PATH%/share/* -o%BIN_BASE_DIR%

:: extract default database
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" %MYSQL_PATH%/data/* -o%DATA%

:: extract ini file examples (missing in 5.6)
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" %MYSQL_PATH%/my-small.ini -o%BIN_BASE_DIR%
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" %MYSQL_PATH%/my-medium.ini -o%BIN_BASE_DIR%
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" %MYSQL_PATH%/my-large.ini -o%BIN_BASE_DIR%


:: move mysql-5.5.62-winx64/data to data
xcopy /s /q /i %DATA%\%MYSQL_PATH% %DATA%
:: delete dir
rmdir /s /q %DATA%\%MYSQL_PATH%

:: copy template with bat scripts and ini file
xcopy /s /q /i %~dp0template %BIN_BASE_DIR%\%MYSQL_PATH%

:: backup initial data directory
xcopy /s /q /i %DATA%\data %DATA%\data-clean