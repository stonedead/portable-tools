setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


:: https://dev.mysql.com/downloads/mysql/
:: https://dev.mysql.com/get/Downloads/MySQL-5.5/mysql-5.5.62-winx64.zip

set FILE=mysql-%MYSQL_VERSION%-%MYSQL_ARCH%.zip

if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)
set BASE_URL=https://dev.mysql.com/get/Downloads/MySQL

set URL=%BASE_URL%-%MYSQL_MAIN_VERSION%/%FILE%

%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"