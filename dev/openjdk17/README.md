# Portable OpenJDK17

## Introduction

JDK 17 is the next LTS version after JDK 11.

Using amazon coretto.

see:
https://aws.amazon.com/de/corretto/faqs/
https://github.com/corretto
https://docs.aws.amazon.com/corretto/latest/corretto-17-ug/downloads-list.html

Installation is trivial (extract zip).