setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=openjdk-corretto-%OPENJDK17_VERSION%_windows-%OPENJDK17_ARCH%_bin.zip

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%BIN_BASE_DIR%

echo Installing OpenJDK17 %OPENJDK17_VERSION%

%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o%DESTINATION_DIR%

:: path in zip file is jdk17.0.3_6, rename to openjdk-17.0.3_x64
ren %DESTINATION_DIR%\%OPENJDK17_ZIP_PATH% openjdk-%OPENJDK17_VERSION%_%OPENJDK17_ARCH%
