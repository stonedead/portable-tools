setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=openjdk-corretto-8u%OPENJDK8_VERSION%_windows-%OPENJDK8_ARCH%_bin.zip

if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)

:: download
%BIN%\wget.exe %OPENJDK8_URL_PATH%%OPENJDK8_URL_FILE% -P "%DOWNLOAD_DIR%"

:: rename
ren "%DOWNLOAD_DIR%\%OPENJDK8_URL_FILE%" "%FILE%"