setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=openjdk-corretto-8u%OPENJDK8_VERSION%_windows-%OPENJDK8_ARCH%_bin.zip

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%BIN_BASE_DIR%

echo Installing OpenJDK8 %OPENJDK8_VERSION%

%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o%DESTINATION_DIR%

:: path in zip file is jdk1.8.0_222, rename to openjdk-8u222_x64
ren %DESTINATION_DIR%\jdk1.8.0_%OPENJDK8_VERSION% openjdk-8u%OPENJDK8_VERSION%_%OPENJDK8_ARCH%
