setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


set FILE_BASE=jdk-%ORACLEJDK_VERSION%_windows-%ORACLEJDK_ARCH%_bin
set FILE=%FILE_BASE%.zip

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

echo Installing OracleJDK %ORACLEJDK_VERSION%-%ORACLEJDK_ARCH%

:: set DESTINATION_DIR=%BIN_BASE_DIR%\jdk-%ORACLEJDK_VERSION%_%ORACLEJDK_ARCH%
set DESTINATION_DIR=%BIN_BASE_DIR%

%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o%DESTINATION_DIR%

ren %DESTINATION_DIR%\jdk-%OPENJDK_VERSION% jdk-%OPENJDK_VERSION%_%ORACLEJDK_ARCH%

goto :eof
:: old version (jdk10) below, no longer used.

set TMP_DIR=%BIN_BASE_DIR%\temp

:: for some reason unpack200.exe is "blocked" for some time after installation (approx 60s)
rmdir /s /q %DESTINATION_DIR%
rmdir /s /q %TMP_DIR%

:: Note: this unpacking requires extra free disk space (700MB), optimization is possible
set RES_ARGS=/OpenDestFolder 0 /ExtractBinary 1 /ExtractManifests 0 /ExtractIcons 0
%BIN%\ResourcesExtract.exe /Source %DOWNLOAD_DIR%\%FILE% /DestFolder %TMP_DIR% %RES_ARGS%

set INSTALLER=jdk-10_100_10.bin
:: "new" case installer
if exist %TMP_DIR%\%INSTALLER% (
  %BIN%\ResourcesExtract.exe /Source %TMP_DIR%\%INSTALLER% /DestFolder %TMP_DIR% %RES_ARGS%
  :: jdk-10_100_10_4_JAVA_CAB10.bin -> tools.zip
  %BIN%\7za.exe x %TMP_DIR%\jdk-10_100_10_4_JAVA_CAB10.bin -o"%TMP_DIR%"
  :: jdk-10_100_10_3_JAVA_CAB9.bin -> src.zip
  %BIN%\7za.exe x %TMP_DIR%\jdk-10_100_10_3_JAVA_CAB9.bin -o%DESTINATION_DIR%\lib
  :: jdk-10_100_10_5_JAVA_CAB11.bin -> COPYRIGHT
  %BIN%\7za.exe x %TMP_DIR%\jdk-10_100_10_5_JAVA_CAB11.bin -o%DESTINATION_DIR%
) else (
  :: old version, no embedded msi
  echo TODO: missing support for old installer...
  exit 1 /B
)
%BIN%\7za.exe x %TMP_DIR%\tools.zip -o%DESTINATION_DIR%

:: unpack pack files..
for %%P in (lib\deploy lib\javaws lib\plugin) do (
  %DESTINATION_DIR%\bin\unpack200.exe  %DESTINATION_DIR%\%%P.pack %DESTINATION_DIR%\%%P.jar
  del %DESTINATION_DIR%\%%P.pack
)

:: clean up extracted resources
rmdir /s /q %TMP_DIR%

:: CHECK: generate classes.jsa?
:: file is not portable, has almost no performance effect, it is not used by default for server vm
:: %DESTINATION_DIR%\bin\java -Xshare:dump
