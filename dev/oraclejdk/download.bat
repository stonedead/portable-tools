setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


:: http://download.oracle.com/otn-pub/java/jdk/10.0.2+13/19aef61b38124481863b1413dce1855f/jdk-10.0.2_windows-x64_bin.exe
:: http://download.oracle.com/otn-pub/java/jdk/11+28/55eed80b163941c8885ad9298e6d786a/jdk-11_windows-x64_bin.zip

set FILE=jdk-%ORACLEJDK_VERSION%_windows-%ORACLEJDK_ARCH%_bin.zip
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)
set BASE_URL=http://download.oracle.com/otn-pub/java/jdk/
set URL=%BASE_URL%/%ORACLEJDK_VERSION%+%ORACLEJDK_BUILD%/%ORACLEJDK_HASH%/%FILE%
%BIN%\wget.exe --header="Cookie: oraclelicense=a" %URL% -P "%DOWNLOAD_DIR%"