# Portable Oracle JDK

## Introduction

These scripts are similar to Oracle JDK 8 (see `oraclejdk8`). 
They download and install the "new and fast" JDK releases (currently 10).

NOTE: *When using this scripts you MUST accept the Oracle Binary Code License Agreement for Java SE*

see: http://www.oracle.com/technetwork/java/javase/terms/license/index.html