setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

:: copy template files to destination
copy %~dp0template_java\show_versions.bat %DEV_BASE_DIR%

:: xcopy creates destination directory if missing
xcopy %~dp0template_java\settings.xml %DEV_BASE_DIR%\userdata\.m2\

:: process template with "replace"
%BIN%\AutoHotkeyU32c.exe %BIN%\replace_env.ahk  %~dp0template_java\je.bat %DEV_BASE_DIR%\je.bat

