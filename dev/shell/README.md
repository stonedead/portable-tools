# Portable Shell Setup

## Introduction

This "application" installs only one batch file that enables usage of all portable tools via command line (i.e. `cmd.exe`).

## Pre-Requisites

`dev\oraclejdk8\install.bat`
`dev\git\install.bat`
`dev\putty\install.bat`
`dev\maven\install.bat`


## Command Setup

calling `%DEV_HOME%\je.bat` will enable java environment (i. e. usage of commands like `java`, `mvn`, `git` etc.)

use specific data will be set to `userdata` e.g. for maven `%DEV_HOME%\userdata\.m2`

Suggested setup is to add `%DEV_HOME%` to windows system path (System Properties - Advanced - Environment Variables..., 
quick access with Win-R `rundll32 sysdm.cpl,EditEnvironmentVariables`)
Then when starting a fresh (and clean) `cmd.exe` simply type `je` to set up path and environment.

`show_versions.bat` will show versions of all used binaries.

## TODO

approach for switching JDK versions.




