@echo off

:: remember old (clean) path, reset if exists
:: note the odd placing of " (required to fix PATH with blanks) - batch is sick.
if defined OLD_PATH (set "PATH=%OLD_PATH%") else (set "OLD_PATH=%PATH%")


:: copy of versions
set GO_VERSION=${GO_VERSION}
set GO_ARCH=${GO_ARCH}
set GIT_VERSION=${GIT_VERSION}
set GIT_ARCH=${GIT_ARCH}
set PUTTY_VERSION=${PUTTY_VERSION}
set PUTTY_ARCH=${PUTTY_ARCH}

set DEV_HOME=%~dp0
:: strip last character (trailing \)
set DEV_HOME=%DEV_HOME:~0,-1%

set BIN_HOME=%DEV_HOME%\bin
set USERDATA=%DEV_HOME%\userdata

:: GO
set GOROOT=%BIN_HOME%\go-%GO_VERSION%_%GO_ARCH%
set GOPATH=%USERDATA%\go\path
set GOCACHE=%USERDATA%\go\cache
set GOTMPDIR=%USERDATA%\go\tmp
set GOENV=%USERDATA%\go\env
set PATH=%GOROOT%\bin;%GOPATH%\bin;%PATH%
:: create empty directories if missing
if not exist %GOPATH% mkdir %GOPATH%
if not exist %GOCACHE% mkdir %GOCACHE%
if not exist %GOTMPDIR% mkdir %GOTMPDIR%

:: PUTTY
set PUTTY_HOME=%BIN_HOME%\putty-%PUTTY_VERSION%_%PUTTY_ARCH%
set PATH=%PUTTY_HOME%;%PATH%

:: GIT
set GIT_HOME=%BIN_HOME%\PortableGit-%GIT_VERSION%_%GIT_ARCH%

:: enable portable global git config ($HOME/.gitconfig)
set HOME=%DEV_HOME%\userdata

:: using path here
set GIT_SSH=plink.exe
:: git 2.5 uses different path, added /usr/bin
set PATH=%GIT_HOME%\bin;%GIT_HOME%\usr\bin;%GIT_HOME%\mingw64\bin\;%PATH%
