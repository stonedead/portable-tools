setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

%BIN%\AutoHotkeyU32c.exe %BIN%\replace_env.ahk %~dp0template_mysql\me.bat %DEV_BASE_DIR%\me.bat
