@echo off

:: remember old (clean) path, reset if exists
:: note the odd placing of " (required to fix PATH with blanks) - batch is sick.
if defined OLD_PATH (set "PATH=%OLD_PATH%") else (set "OLD_PATH=%PATH%")


:: copy of versions
set JDK_VERSION=8u${OPENJDK8_VERSION}
set JDK_ARCH=${OPENJDK8_ARCH}
set JDK_BRAND=openjdk
set GIT_VERSION=${GIT_VERSION}
set GIT_ARCH=${GIT_ARCH}
set PUTTY_VERSION=${PUTTY_VERSION}
set PUTTY_ARCH=${PUTTY_ARCH}
set JRUBY_VERSION=${JRUBY_VERSION}

:: allow version override
if not "%1"=="" (set JDK_VERSION=%1)
if not "%2"=="" (set GIT_VERSION=%2)

set DEV_HOME=%~dp0
:: strip last character (trailing \)
set DEV_HOME=%DEV_HOME:~0,-1%
set BIN_HOME=%DEV_HOME%\bin

:: JAVA
set JAVA_HOME=%BIN_HOME%\%JDK_BRAND%-%JDK_VERSION%_%JDK_ARCH%
set PATH=%JAVA_HOME%\bin;%PATH%

:: RUBY
set JRUBY_HOME=%BIN_HOME%\jruby-%JRUBY_VERSION%
set PATH=%JRUBY_HOME%\bin;%PATH%
:: improve portability
:: setting GEM_HOME avoids messing up binary path %JRUBY_HOME%\lib\ruby\gems
:: but gems like rails are BROKEN
:: set GEM_HOME=%DEV_HOME%\userdata\ruby-gems
:: set GEM_SPEC_CACHE=%GEM_HOME%\spec_cache

:: just try to fix creation of any files in home directory
set GEM_SPEC_CACHE=%JRUBY_HOME%\lib\ruby\gems\spec_cache
:: bundles, stay with anti-pattern binary path.
:: set BUNDLE_PATH=%JRUBY_HOME%\lib\ruby\bundle  :: this causes gems in bundle\gems and scripts in bundle\bin
:: set BUNDLE_CACHE_PATH=%BUNDLE_PATH%\cache

:: PUTTY
set PUTTY_HOME=%BIN_HOME%\putty-%PUTTY_VERSION%_%PUTTY_ARCH%
set PATH=%PUTTY_HOME%;%PATH%

:: GIT
set GIT_HOME=%BIN_HOME%\PortableGit-%GIT_VERSION%_%GIT_ARCH%

:: enable portable global git config ($HOME/.gitconfig)
set HOME=%DEV_HOME%\userdata

:: using path here
set GIT_SSH=plink.exe
:: git 2.5 uses different path, added /usr/bin
set PATH=%GIT_HOME%\bin;%GIT_HOME%\usr\bin;%GIT_HOME%\mingw64\bin\;%PATH%
