@echo off

:: remember old (clean) path, reset if exists
:: note the odd placing of " (required to fix PATH with blanks) - batch is sick.
if defined OLD_PATH (set "PATH=%OLD_PATH%") else (set "OLD_PATH=%PATH%")


:: copy of versions
:: set JDK_VERSION=${ORACLEJDK8_VERSION}
:: set JDK_ARCH=${ORACLEJDK8_ARCH}
:: set JDK_BRAND=jdk
set JDK_VERSION=8u${OPENJDK8_VERSION}
set JDK_ARCH=${OPENJDK8_ARCH}
set JDK_BRAND=openjdk
set GIT_VERSION=${GIT_VERSION}
set GIT_ARCH=${GIT_ARCH}
set MAVEN_VERSION=${MAVEN_VERSION}
set PUTTY_VERSION=${PUTTY_VERSION}
set PUTTY_ARCH=${PUTTY_ARCH}

:: allow version override
if not "%1"=="" (set JDK_VERSION=%1)
if not "%2"=="" (set MAVEN_VERSION=%2)
if not "%3"=="" (set GIT_VERSION=%3)

set DEV_HOME=%~dp0
:: strip last character (trailing \)
set DEV_HOME=%DEV_HOME:~0,-1%
set BIN_HOME=%DEV_HOME%\bin

:: JAVA
set JAVA_HOME=%BIN_HOME%\%JDK_BRAND%-%JDK_VERSION%_%JDK_ARCH%
set PATH=%JAVA_HOME%\bin;%PATH%

:: MAVEN
::set MAVEN_OPTS=-Xmx384m
set MAVEN_HOME=%BIN_HOME%\apache-maven-%MAVEN_VERSION%
set PATH=%MAVEN_HOME%\bin;%PATH%
:: relocate .m2 with some tricks to M2_ROOT (requires patched mvn.cmd)
set M2_ROOT=%DEV_HOME%/userdata/.m2
:: note: M2_ROOT will be referenced in settings.xml
set MAVEN_EXTRA_ARGS=-s "%M2_ROOT%/settings.xml" 

:: PUTTY
set PUTTY_HOME=%BIN_HOME%\putty-%PUTTY_VERSION%_%PUTTY_ARCH%
set PATH=%PUTTY_HOME%;%PATH%

:: GIT
set GIT_HOME=%BIN_HOME%\PortableGit-%GIT_VERSION%_%GIT_ARCH%

:: this trick makes ssh keys "visible". no longer required - using plink/pageant
:: set HOME=%USERPROFILE%

:: enable portable global git config ($HOME/.gitconfig)
set HOME=%DEV_HOME%\userdata

:: using path here
set GIT_SSH=plink.exe
:: git 2.5 uses different path, added /usr/bin
set PATH=%GIT_HOME%\bin;%GIT_HOME%\usr\bin;%GIT_HOME%\mingw64\bin\;%PATH%
