@echo off
if defined MAVEN_HOME (
  ECHO MAVEN_HOME=%MAVEN_HOME%
  :: use "call" to avoid that mvn.bat exits too early.
  call mvn --version
  ECHO.
)
if defined JAVA_HOME (
  :: MAVEN is already showing java version
  if not defined MAVEN_HOME (
    ECHO JAVA_HOME=%JAVA_HOME%
    java -version
    ECHO.
  )
)
if defined GIT_HOME (
  ECHO GIT_HOME=%GIT_HOME%
  git --version
  ECHO.
)
if defined PUTTY_HOME (
  ECHO PUTTY_HOME=%PUTTY_HOME%
  plink --version
  ECHO.
)