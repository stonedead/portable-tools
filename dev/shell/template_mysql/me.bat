@echo off

set DEV_HOME=%~dp0
:: strip last character (trailing \)
set DEV_HOME=%DEV_HOME:~0,-1%
set BIN_HOME=%DEV_HOME%\bin

:: copy of versions
set MYSQL_VERSION=${MYSQL_VERSION}
set MYSQL_ARCH=${MYSQL_ARCH}

:: MYSQL
set MYSQL_HOME=%BIN_HOME%\mysql-%MYSQL_VERSION%-%MYSQL_ARCH%
set PATH=%MYSQL_HOME%;%PATH%
