@echo off

set DEV_HOME=%~dp0
:: strip last character (trailing \)
set DEV_HOME=%DEV_HOME:~0,-1%
set BIN_HOME=%DEV_HOME%\bin

:: copy of versions
set MARIADB_VERSION=${MARIADB_VERSION}
set MARIADB_ARCH=${MARIADB_ARCH}

:: MARIADB
set MARIADB_HOME=%BIN_HOME%\mariadb-%MARIADB_VERSION%-%MARIADB_ARCH%
set PATH=%MARIADB_HOME%;%PATH%
