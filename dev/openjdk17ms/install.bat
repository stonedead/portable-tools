setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=microsoft-jdk-%OPENJDK17MS_VERSION%-windows-%OPENJDK17MS_ARCH%.zip

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%BIN_BASE_DIR%

echo Installing OpenJDK17 %OPENJDK17MS_VERSION%

%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o%DESTINATION_DIR%

ren %DESTINATION_DIR%\%OPENJDK17MS_ZIP_PATH% jdk-ms-%OPENJDK17MS_VERSION%_%OPENJDK17MS_ARCH%
