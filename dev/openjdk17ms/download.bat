setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=microsoft-jdk-%OPENJDK17MS_VERSION%-windows-%OPENJDK17MS_ARCH%.zip

if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)

:: download
%BIN%\wget.exe https://aka.ms/download-jdk/%FILE% -P "%DOWNLOAD_DIR%"
