setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


:: https://www.jruby.org/download
:: https://repo1.maven.org/maven2/org/jruby/jruby-dist/9.2.5.0/jruby-dist-9.2.5.0-bin.tar.gz

set FILE=jruby-dist-%JRUBY_VERSION%-bin.tar.gz

if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)
set BASE_URL=https://repo1.maven.org/maven2/org/jruby/jruby-dist

set URL=%BASE_URL%/%JRUBY_VERSION%/%FILE%

%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"