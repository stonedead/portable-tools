# Portable JRuby

## Introduction

Ruby (and JRuby) are considered as ugly environment (slow, sloppy and hard to control).

The purpose of this setup is to put all the dirt in a reproducible (and disposable) box and use if for toy projects 
or for tools that were (sadly) created with ruby. 
JRuby avoids native binaries and improves portability (but reduces compatibility).


## Shell Install

Installation is trivial (extract archive).

`dev\jruby\install.bat` \
`dev\shell\install_jruby.bat`

## Shell Usage

calling `%DEV_HOME%\re.bat` will enable jruby environment.

suggested command usage (avoid possibly broken batch files with absolute paths)

`jruby -S gem` \
`jruby -S bundle` \
`jruby -S rake` \
`jruby -S rails` \
etc.

## Oddities

Gems are installed in binary path (`%JRUBY_HOME%\lib\ruby\gems`) - this is ugly.

When "fixing" this with environment like certain gems (e.g. rails) get broken, so I ignore this for now.


