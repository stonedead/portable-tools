setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=jruby-dist-%JRUBY_VERSION%-bin.tar.gz

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%BIN_BASE_DIR%

echo Installing JRuby %JRUBY_VERSION%

%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -so | %BIN%\7za.exe x -aoa -si -ttar -o%DESTINATION_DIR%


