@echo off

setlocal

set DEV_HOME=%~dp0
:: strip last character (trailing \)
set DEV_HOME=%DEV_HOME:~0,-1%

set PUTTY_VERSION=${PUTTY_VERSION}
set PUTTY_ARCH=${PUTTY_ARCH}

set PAGEANT=%DEV_HOME%\bin\putty-%PUTTY_VERSION%_%PUTTY_ARCH%\pageant.exe
set KEY_DIR=%DEV_HOME%\userdata\keys

:: find all keys and load via commandline.
:: first invocation will start pageant if necessary
for %%f in (%KEY_DIR%\*.ppk) do (
  echo loading key %%f
  start %PAGEANT% --encrypted %%f
  :: avoid errors when called too fast
  timeout 1
)

:::::: this is broken ::::::
:: for %%f in (%KEY_DIR%\*) do (
::   :: this is not working? see "cmd help set" (requires delayed expansion with "cmd.exe /v" and !KEYS! WTF?)
::   set KEYS=!KEYS! %%f
:: )

