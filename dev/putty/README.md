# Portable Putty

## Introduction

The headline is lie. Putty is not portable as it uses the registry on windows.
see `reg query HKCU\Software\SimonTatham\PuTTY /s`

Putty can use files in Linux but this "gift" is not available for the poor windows users.
For some reasons portable operation is not even listed on the "wishlist".

Getting portable operation requires to switch to forks like kitty. 
Using putty means staying on the save side and accepting portability issues.

The author suggests registry export/import with batch files but this is cumbersome and ugly.

https://the.earth.li/~sgtatham/putty/0.70/htmldoc/Chapter4.html#config-file


## Setup

The main use case is using the putty agent (`pageant.exe`) to provide keys for git via ssh. 
Use `puttygen.exe` to convert keys from openssh format to putty format. 

`load_keys.bat` will load all keys from `%DEV_HOME%\userdata\keys`. `pageant.exe` will prompt for passwords.

This is "almost" portable, to overcome the missing link problem 
(cannot press "y" as git is not forwarding stdin) invoke once "manually":

`plink.exe git@host.com` and confirm with "y" - then `git clone ssh://git@host.com/repo` will work as expected.

Note: this is a "known issue" of git for a long time (2015-08). see https://github.com/git-for-windows/build-extra/blob/master/ReleaseNotes.md

