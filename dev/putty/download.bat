setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

:: https://the.earth.li/~sgtatham/putty/0.70/w32/putty.zip
set FILE=putty-%PUTTY_VERSION%_%PUTTY_ARCH%.zip
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)

set BASE_URL=http://the.earth.li/~sgtatham/putty
set URL=%BASE_URL%/%PUTTY_VERSION%/%PUTTY_ARCH%/putty.zip
%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"
:: add version and arch to name
ren "%DOWNLOAD_DIR%\putty.zip" %FILE%