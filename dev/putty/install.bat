setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=putty-%PUTTY_VERSION%_%PUTTY_ARCH%.zip

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%BIN_BASE_DIR%\putty-%PUTTY_VERSION%_%PUTTY_ARCH%

%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o%DESTINATION_DIR%

:: process template with "replace"
%BIN%\AutoHotkeyU32c.exe %BIN%\replace_env.ahk  %~dp0template\load_keys.bat %DEV_BASE_DIR%\load_keys.bat