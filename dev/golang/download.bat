setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

:: https://golang.org/dl/ links to
:: https://dl.google.com/go/go1.11.windows-amd64.zip
:: https://dl.google.com/go/go1.11.windows-386.zip

set FILE=go%GO_VERSION%.windows-%GO_ARCH%.zip

if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)
set BASE_URL=https://dl.google.com/go/

set URL=%BASE_URL%/%FILE%

%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"