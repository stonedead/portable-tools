# Portable Go

## Introduction

Installation is trivial (extract zip).

## Shell Setup

`dev\git\install.bat` \
`dev\putty\install.bat` \
`dev\golang\install.bat` \
`dev\shell\install_go.bat`

## Shell Usage

calling `%DEV_HOME%\ge.bat` will enable go environment (i.e. usage of commands like `go`, `gofmt`, `godoc`, `git` etc.)