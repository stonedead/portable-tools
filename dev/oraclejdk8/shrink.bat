setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set DESTINATION_DIR=%BIN_BASE_DIR%\jdk-%ORACLEJDK8_VERSION%_%ORACLEJDK8_ARCH%

:: clean javafx (scattered!)
del /Q %DESTINATION_DIR%\javafx-src.zip
del /Q %DESTINATION_DIR%\bin\javafxpackager.exe
del /Q %DESTINATION_DIR%\jre\bin\jfx*.*
del /Q %DESTINATION_DIR%\jre\bin\javafx*.*
del /Q %DESTINATION_DIR%\jre\bin\prism*.*
del /Q %DESTINATION_DIR%\jre\bin\glass.dll
del /Q %DESTINATION_DIR%\jre\bin\decora_sse.dll
del /Q %DESTINATION_DIR%\jre\bin\fxplugins.dll
del /Q %DESTINATION_DIR%\jre\bin\glib-lite.dll
del /Q %DESTINATION_DIR%\jre\bin\gstreamer-lite.dll
del /Q %DESTINATION_DIR%\jre\lib\jfxswt.jar
del /Q %DESTINATION_DIR%\jre\lib\javafx.properties
del /Q %DESTINATION_DIR%\jre\lib\ext\jfxrt.jar
del /Q %DESTINATION_DIR%\lib\ant-javafx.jar
del /Q %DESTINATION_DIR%\lib\javafx-mx.jar

:: clean javadb (6MB), obsolete with 8u181
rmdir /s /q %DESTINATION_DIR%\db

:: clean java plugin (source of pain and bugs)
del /Q %DESTINATION_DIR%\jre\bin\javacpl.*
del /Q %DESTINATION_DIR%\jre\bin\jp2*.*
del /Q %DESTINATION_DIR%\jre\bin\ssv*.*
del /Q %DESTINATION_DIR%\jre\bin\eula.dll
del /Q %DESTINATION_DIR%\jre\lib\plugin.jar
rmdir /s /q %DESTINATION_DIR%\jre\bin\dtplugin
rmdir /s /q %DESTINATION_DIR%\jre\bin\plugin2

:: clean webstart
del /Q %DESTINATION_DIR%\bin\javaws.exe
del /Q %DESTINATION_DIR%\jre\bin\wsdetect.dll
del /Q %DESTINATION_DIR%\jre\lib\deploy.jar
del /Q %DESTINATION_DIR%\jre\lib\javaws.jar
rmdir /s /q %DESTINATION_DIR%\jre\lib\deploy


:: clean mission control (61MB)
rmdir /s /q %DESTINATION_DIR%\lib\missioncontrol
del /Q %DESTINATION_DIR%\bin\jmc.*

:: clean visual jvm (36MB)
rmdir  /s /q %DESTINATION_DIR%\lib\visualvm
del /Q %DESTINATION_DIR%\bin\jvisualvm.exe
