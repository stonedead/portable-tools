setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat


set FILE_BASE=jdk-%ORACLEJDK8_VERSION%-windows-%ORACLEJDK8_ARCH%
set FILE=%FILE_BASE%.exe

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

echo Installing OracleJDK %ORACLEJDK8_VERSION%-%ORACLEJDK8_ARCH%

set DESTINATION_DIR=%BIN_BASE_DIR%\jdk-%ORACLEJDK8_VERSION%_%ORACLEJDK8_ARCH%

set TMP_DIR=%BIN_BASE_DIR%\temp

:: for some reason unpack200.exe is "blocked" for some time after installation (approx 60s)
rmdir /s /q %DESTINATION_DIR%
rmdir /s /q %TMP_DIR%

:: Note: this unpacking requires extra free disk space (700MB), optimization is possible
set RES_ARGS=/OpenDestFolder 0 /ExtractBinary 1 /ExtractManifests 0 /ExtractIcons 0
%BIN%\ResourcesExtract.exe /Source %DOWNLOAD_DIR%\%FILE% /DestFolder %TMP_DIR% %RES_ARGS%
:: jdk-8u171-windows-x64_100_10.bin
set INSTALLER=%FILE_BASE%_100_10.bin
:: "new" case installer
if exist %TMP_DIR%\%INSTALLER% (
  %BIN%\ResourcesExtract.exe /Source %TMP_DIR%\%INSTALLER% /DestFolder %TMP_DIR% %RES_ARGS%
  :: jdk-8u171-windows-x64_100_10_111_JAVA_CAB10.bin -> tools.zip
  %BIN%\7za.exe x %TMP_DIR%\%FILE_BASE%_100_10_111_JAVA_CAB10.bin -o"%TMP_DIR%"
  :: jdk-8u171-windows-x64_100_10_110_JAVA_CAB9.bin -> src.zip
  %BIN%\7za.exe x %TMP_DIR%\%FILE_BASE%_100_10_110_JAVA_CAB9.bin -o%DESTINATION_DIR%
  :: jdk-8u171-windows-x64_100_10_112_JAVA_CAB11.bin -> COPYRIGHT
  %BIN%\7za.exe x %TMP_DIR%\%FILE_BASE%_100_10_112_JAVA_CAB11.bin -o%DESTINATION_DIR%
) else (
  :: old version, no embedded msi
  echo TODO: missing support for old installer...
  exit 1 /B
)
%BIN%\7za.exe x %TMP_DIR%\tools.zip -o%DESTINATION_DIR%

:: unpack pack files..
for %%P in (jre\lib\charsets jre\lib\deploy jre\lib\ext\localedata jre\lib\javaws jre\lib\jsse jre\lib\plugin jre\lib\rt lib\tools) do (
  %DESTINATION_DIR%\bin\unpack200.exe  %DESTINATION_DIR%\%%P.pack %DESTINATION_DIR%\%%P.jar
  del %DESTINATION_DIR%\%%P.pack
)

:: clean up extracted resources
rmdir /s /q %TMP_DIR%

:: CHECK: generate classes.jsa?
:: file is not portable, has almost no performance effect, it is not used by default for server vm
:: %DESTINATION_DIR%\bin\java -Xshare:dump
