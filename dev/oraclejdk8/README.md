# Portable Oracle JDK 8

## Introduction

JDK is portable, but Oracle will not make the according "image" available (for JDK8).

This scripts will download installer and extract JDK "image" files. The "innovative" way to extract 
the embedded CAB files uses Nirsoft ResourceExtract.

Certain components like webstart and plugin are not portable.
These components will be ignored - they are useless and caused endless security issues.
Note that OpenJDK resolved this by omitting these parts.

There is also a "brute force" cleanup script to slim down JDK (`shrink.bat`). Modular JDK9 will give you better options.

NOTE: *When using this scripts you MUST accept the Oracle Binary Code License Agreement for Java SE*

see: http://www.oracle.com/technetwork/java/javase/terms/license/index.html