setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

:: JDK-8 http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html
:: http://download.oracle.com/otn-pub/java/jdk/8u171-b11/512cd62ec5174c3487ac17c61aaa89e8/jdk-8u171-windows-x64.exe

:: Archive http://www.oracle.com/technetwork/java/javase/archive-139210.html
:: http://download.oracle.com/otn/java/jdk/8u112-b15/jdk-8u112-windows-i586.exe
:: Note: "Downloading these releases requires an oracle.com account."

set FILE=jdk-%ORACLEJDK8_VERSION%-windows-%ORACLEJDK8_ARCH%.exe
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)
set BASE_URL=http://download.oracle.com/otn/java/jdk/
set URL=%BASE_URL%/%ORACLEJDK8_VERSION%-%ORACLEJDK8_BUILD%/%ORACLEJDK8_HASH%/%FILE%
%BIN%\wget.exe --header="Cookie: oraclelicense=a" %URL% -P "%DOWNLOAD_DIR%"