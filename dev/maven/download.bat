setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

:: http://www-eu.apache.org/dist/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz
set FILE=apache-maven-%MAVEN_VERSION%-bin.tar.gz
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)

set BASE_URL=https://dlcdn.apache.org/maven/maven-3
set URL=%BASE_URL%/%MAVEN_VERSION%/binaries/%FILE%
:: certificate expired, great!
%BIN%\wget.exe --no-check-certificate %URL% -P "%DOWNLOAD_DIR%"