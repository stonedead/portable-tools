setlocal

call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=apache-maven-%MAVEN_VERSION%-bin.tar.gz

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%BIN_BASE_DIR%

:: tar.gz extract with "pipe"
%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -so | %BIN%\7za.exe x -aoa -si -ttar -o%DESTINATION_DIR%

:: patch mvn.cmd, add %MAVEN_EXTRA_ARGS% (e.g. to enable custom settings file)
set MAVEN_BIN_DIR=%DESTINATION_DIR%\apache-maven-%MAVEN_VERSION%\bin
ren %MAVEN_BIN_DIR%\mvn.cmd mvn.cmd.orig
:: using %% to avoid "variable evaluation"
%BIN%\AutoHotkeyU32c.exe %BIN%\replace.ahk %MAVEN_BIN_DIR%\mvn.cmd.orig %MAVEN_BIN_DIR%\mvn.cmd %%MAVEN_CMD_LINE_ARGS%% "%%MAVEN_CMD_LINE_ARGS%% %%MAVEN_EXTRA_ARGS%%"