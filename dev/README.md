# Portable Development Tools

## Introduction

The primary tools are for Java development (other language may follow).

## Shell Setup

`shell` will help to create command line setup (Initial basic setup starts with java + maven + git + putty.).

it depends on installation of according tools. e.g.

```
dev\openjdk8\install.bat
dev\maven\install.bat
dev\putty\install.bat
dev\git\install.bat
dev\shell\install_java.bat
```

possible useful subsets: java+maven (maven depends on java), putty+git (git depends on putty for ssh keys)

## IDE Setup

current IDE setup exists for Intellij IDEA (`idea`). It depends on the same installed products like `shell`.

```
dev\idea\install.bat
```

## Custom Installation Path

to set custom paths create `dev\user_paths.bat` before installation:

```
set DOWNLOAD_DIR=j:\portable_tools\download
:: this is clumsy, 3 variables required
set DEV_BASE_DIR=j:\portable_dev
set BIN_BASE_DIR=%DEV_BASE_DIR%\bin
set DEV_USER_DIR=%DEV_BASE_DIR%\userdata
set IDE_BASE_DIR=%DEV_BASE_DIR%\ide
```