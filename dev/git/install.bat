setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=PortableGit-%GIT_VERSION%-%GIT_ARCH_FILE%.7z.exe
if not %GIT_BUILD_NR%==1 set FILE=PortableGit-%GIT_VERSION%.%GIT_BUILD_NR%-%GIT_ARCH_FILE%.7z.exe

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%BIN_BASE_DIR%\PortableGit-%GIT_VERSION%_%GIT_ARCH%

rmdir /s /q %DESTINATION_DIR%

%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o%DESTINATION_DIR%
