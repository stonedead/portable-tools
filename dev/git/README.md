# Portable Git

this is trivial. download url:
https://github.com/git-for-windows/git/releases/

## SSH Keys

Multiple flavors exist to solve this. The following approach was used.

Git will use putty plink for ssh protocol with `set GIT_SSH=plink.exe`

Putty will load keys from putty agent (see `putty` for details).
