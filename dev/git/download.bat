setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

:: https://github.com/git-for-windows/git/releases/download/v2.18.0.windows.1/PortableGit-2.18.0-64-bit.7z.exe
:: new variant with build in filename (only if build > 1)
:: https://github.com/git-for-windows/git/releases/download/v2.24.1.windows.2/PortableGit-2.24.1.2-64-bit.7z.exe
:: new arm64 urls
:: https://github.com/git-for-windows/git/releases/download/v2.47.1.windows.1/PortableGit-2.47.1-arm64.7z.exe
set FILE=PortableGit-%GIT_VERSION%-%GIT_ARCH_FILE%.7z.exe
if not %GIT_BUILD_NR%==1 set FILE=PortableGit-%GIT_VERSION%.%GIT_BUILD_NR%-%GIT_ARCH_FILE%.7z.exe

if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)
set BASE_URL=https://github.com/git-for-windows/git/releases/download/
set URL=%BASE_URL%/v%GIT_VERSION%.windows.%GIT_BUILD_NR%/%FILE%
%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"