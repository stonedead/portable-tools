:: install paths

set DEV_BASE_DIR=%INSTALL_BASE_DIR%\dev

:: path for "shell" binaries
set BIN_BASE_DIR=%INSTALL_BASE_DIR%\dev\bin
:: path for IDEs
set IDE_BASE_DIR=%INSTALL_BASE_DIR%\dev\ide


:: will hold user stuff (e.g. .m2 and .ideaIC)
set DEV_USER_DIR=%INSTALL_BASE_DIR%\dev\userdata
