:: product versions defaults

:::: ORACLE JAVA ::::
:: set ORACLEJDK8_ARCH=i586
set ORACLEJDK8_ARCH=x64
set ORACLEJDK8_VERSION=8u261
set ORACLEJDK8_BUILD=b12
set ORACLEJDK8_HASH=a4634525489241b9a9e1aa73d9e118e6

set ORACLEJDK_ARCH=x64
set ORACLEJDK_VERSION=22.0.2
set ORACLEJDK_BUILD=9
set ORACLEJDK_HASH=c9ecb94cd31b495da20a27d4581645e8

:::: OPENJDK ::::
:: https://jdk.java.net/23/
set OPENJDK_MAJOR_VERSION=23
set OPENJDK_VERSION=23.0.1
set OPENJDK_BUILD=11
set OPENJDK_HASH=c28985cbf10d4e648e4004050f8781aa

:::: OPENJDK8 (corretto) ::::
:: https://docs.aws.amazon.com/corretto/latest/corretto-8-ug/downloads-list.html
:: https://github.com/corretto/corretto-8/releases
set OPENJDK8_VERSION=432
set OPENJDK8_ARCH=x64
:: set OPENJDK8_ARCH=x86
set OPENJDK8_URL_PATH=https://corretto.aws/downloads/resources/8.432.06.1/
set OPENJDK8_URL_FILE=amazon-corretto-8.432.06.1-windows-%OPENJDK8_ARCH%-jdk.zip

:::: OPENJDK11 (corretto) ::::
:: https://docs.aws.amazon.com/corretto/latest/corretto-11-ug/downloads-list.html
:: https://github.com/corretto/corretto-11/releases
set OPENJDK11_ARCH=x64
set OPENJDK11_VERSION=11.0.25
set OPENJDK11_URL_PATH=https://corretto.aws/downloads/resources/11.0.25.9.1/
:: only x64 available
set OPENJDK11_URL_FILE=amazon-corretto-11.0.25.9.1-windows-%OPENJDK11_ARCH%-jdk.zip
set OPENJDK11_ZIP_PATH=jdk11.0.25_9

:::: OPENJDK17 (corretto) ::::
:: https://docs.aws.amazon.com/corretto/latest/corretto-17-ug/downloads-list.html
:: https://github.com/corretto/corretto-17/releases
set OPENJDK17_ARCH=x64
set OPENJDK17_VERSION=17.0.13
set OPENJDK17_URL_PATH=https://corretto.aws/downloads/resources/17.0.13.11.1/
:: only x64 available
set OPENJDK17_URL_FILE=amazon-corretto-17.0.13.11.1-windows-%OPENJDK17_ARCH%-jdk.zip
set OPENJDK17_ZIP_PATH=jdk17.0.13_11

:::: OPENJDK17 (Microsoft) ::::
:: https://docs.microsoft.com/en-us/java/openjdk/download
:: https://aka.ms/download-jdk/microsoft-jdk-17.0.11-windows-x64.zip
:: https://aka.ms/download-jdk/microsoft-jdk-17.0.11-windows-aarch64.zip
set OPENJDK17MS_ARCH=x64
if %PROCESSOR_ARCHITECTURE%==ARM64 set OPENJDK17MS_ARCH=aarch64
:: update delayed
set OPENJDK17MS_VERSION=17.0.13
set OPENJDK17MS_ZIP_PATH=jdk-17.0.13+11

:::: OPENJDK21 (corretto) ::::
:: https://docs.aws.amazon.com/corretto/latest/corretto-21-ug/downloads-list.html
:: https://github.com/corretto/corretto-21/releases
set OPENJDK21_VERSION=21.0.5
set OPENJDK21_URL_PATH=https://corretto.aws/downloads/resources/21.0.5.11.1/
:: only x64 available
set OPENJDK21_URL_FILE=amazon-corretto-21.0.5.11.1-windows-x64-jdk.zip
set OPENJDK21_ZIP_PATH=jdk21.0.5_11

:::: GIT :::
:: https://git-scm.com/download/win
set GIT_VERSION=2.48.1
set GIT_BUILD_NR=1
::set GIT_ARCH=32
set GIT_ARCH_FILE=64-bit
set GIT_ARCH=x64
if %PROCESSOR_ARCHITECTURE%==ARM64 (
  set GIT_ARCH_FILE=arm64
  set GIT_ARCH=arm64
)

:::: MAVEN ::::
:: https://maven.apache.org/download.cgi
set MAVEN_VERSION=3.9.9

:::: INTELLIJ IDEA ::::
:: https://www.jetbrains.com/idea/download/
set IDEA_VERSION=2024.3.3
:: could separate settings per version
:: set IDEA_PROFILE=.IdeaIC201801
set IDEA_PROFILE=.IdeaIC

:::: PUTTY ::::
:: https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
set PUTTY_VERSION=0.83
set PUTTY_ARCH=w64
if %PROCESSOR_ARCHITECTURE%==ARM64 set PUTTY_ARCH=wa64

:::: GO ::::
:: https://go.dev/dl/
set GO_VERSION=1.24.0
set GO_ARCH=amd64
if %PROCESSOR_ARCHITECTURE%==ARM64 set GO_ARCH=arm64

:::: JRUBY ::::
:: https://www.jruby.org/files/downloads/index.html
set JRUBY_VERSION=9.4.9.0

:::: MYSQL ::::
:: https://dev.mysql.com/downloads/mysql/
:: support 5.5 ended 2018-12
set MYSQL_MAIN_VERSION=5.5
set MYSQL_SUB_VERSION=62
:: support 5.6 ended 2021-02
:: set MYSQL_MAIN_VERSION=5.6
:: set MYSQL_SUB_VERSION=51
:: support 5.7 will end 2023-20
:: set MYSQL_MAIN_VERSION=5.7
:: set MYSQL_SUB_VERSION=35
set MYSQL_VERSION=%MYSQL_MAIN_VERSION%.%MYSQL_SUB_VERSION%
:: set MYSQL_ARCH=win32
set MYSQL_ARCH=winx64

:::: MARIADB ::::
:: https://downloads.mariadb.org/mariadb/+releases/
::set MARIADB_VERSION=10.3.39
::set MARIADB_VERSION=10.4.32
set MARIADB_VERSION=10.5.23
::set MARIADB_VERSION=10.6.16
:: set MYSQL_ARCH=win32
set MARIADB_ARCH=winx64