setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

:: set FILE=openjdk-%OPENJDK_VERSION%+%OPENJDK_BUILD%_windows-x64_bin.zip
set FILE=openjdk-%OPENJDK_VERSION%_windows-x64_bin.zip

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%BIN_BASE_DIR%

echo Installing OpenJDK %OPENJDK_VERSION%

%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o%DESTINATION_DIR%

ren %DESTINATION_DIR%\jdk-%OPENJDK_VERSION% openjdk-%OPENJDK_VERSION%_x64

:: tar.gz extract with "pipe"
:: %BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -so | %BIN%\7za.exe x -aoa -si -ttar -o%DESTINATION_DIR%