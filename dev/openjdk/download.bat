setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

:: this url schema is changing frequently...
:: https://download.java.net/java/GA/jdk11/28/GPL/openjdk-11+28_windows-x64_bin.zip
:: and again for 11.0.1
:: https://download.java.net/java/GA/jdk11/13/GPL/openjdk-11.0.1_windows-x64_bin.zip
:: and again for JDK12
:: https://download.java.net/java/GA/jdk12/GPL/openjdk-12_windows-x64_bin.zip
:: and again for 12.0.1
:: https://download.java.net/java/GA/jdk12.0.1/69cfe15208a647278a19ef0990eea691/12/GPL/openjdk-12.0.1_windows-x64_bin.zip

:: https://download.java.net/java/GA/jdk10/10.0.1/fb4372174a714e6b8c52526dc134031e/10//openjdk-openjdk-10.0.1_windows-x64_bin.tar.gz
:: https://download.java.net/java/GA/jdk9/9.0.4/binaries/openjdk-9.0.4_windows-x64_bin.tar.gz


:: set FILE=openjdk-%OPENJDK_VERSION%+%OPENJDK_BUILD%_windows-x64_bin.zip
set FILE=openjdk-%OPENJDK_VERSION%_windows-x64_bin.zip
if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)
set BASE_URL=https://download.java.net/java/GA

if "%OPENJDK_HASH%"=="" (
  if "%OPENJDK_BUILD%"=="" (
    set URL=%BASE_URL%/jdk%OPENJDK_MAJOR_VERSION%/%OPENJDK_BUILD%/GPL/%FILE%
  ) else (
    set URL=%BASE_URL%/jdk%OPENJDK_MAJOR_VERSION%/GPL/%FILE%
  )
) else (
   set URL=%BASE_URL%/jdk%OPENJDK_VERSION%/%OPENJDK_HASH%/%OPENJDK_BUILD%/GPL/%FILE%
)
%BIN%\wget.exe %URL% -P "%DOWNLOAD_DIR%"