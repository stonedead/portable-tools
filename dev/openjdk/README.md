# Portable OpenJDK

## Introduction

Starting with JDK9 oracle delivers an official build of OpenJDK for Windows.

see: 
http://jdk.java.net/9/
http://jdk.java.net/10/
http://jdk.java.net/11/

Installation is trivial (extract tar.gz or zip).