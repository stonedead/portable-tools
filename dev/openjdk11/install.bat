setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist  %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=openjdk-corretto-%OPENJDK11_VERSION%_windows-%OPENJDK11_ARCH%_bin.zip

if not exist %DOWNLOAD_DIR%\%FILE% call %~dp0download.bat

set DESTINATION_DIR=%BIN_BASE_DIR%

echo Installing OpenJDK11 %OPENJDK11_VERSION%

%BIN%\7za.exe x "%DOWNLOAD_DIR%\%FILE%" -o%DESTINATION_DIR%

:: path in zip file is jdk11.0.5_10, rename to openjdk-11.0.5_x64
ren %DESTINATION_DIR%\%OPENJDK11_ZIP_PATH% openjdk-%OPENJDK11_VERSION%_%OPENJDK11_ARCH%
