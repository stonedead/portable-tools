# Portable OpenJDK11

## Introduction

Since Oracle JDK11 went "commercial" multiple alternative builds became available.

For this project amazon coretto was selected for best feature set (validated with TCK, signed binaries, download without login).

see:
https://aws.amazon.com/de/corretto/faqs/
https://github.com/corretto
https://docs.aws.amazon.com/corretto/latest/corretto-11-ug/downloads-list.html

Installation is trivial (extract zip).