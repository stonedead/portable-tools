# Portable OpenJDK17

## Introduction

JDK 21 is the next LTS version after JDK 17.

Using amazon coretto.

see:
https://aws.amazon.com/de/corretto/faqs/
https://github.com/corretto
https://docs.aws.amazon.com/corretto/latest/corretto-21-ug/downloads-list.html

Installation is trivial (extract zip).