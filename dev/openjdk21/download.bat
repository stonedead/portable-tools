setlocal
call %~dp0..\..\setup\settings.bat
call %~dp0..\default_versions.bat
if exist %~dp0..\user_versions.bat call %~dp0..\user_versions.bat
call %~dp0..\default_paths.bat
if exist %~dp0..\user_paths.bat call %~dp0..\user_paths.bat

set FILE=openjdk-corretto-%OPENJDK21_VERSION%_windows-x64_bin.zip

if exist "%DOWNLOAD_DIR%\%FILE%" (
  echo file exits: %FILE%
  exit /B 0
)

:: download
%BIN%\wget.exe %OPENJDK21_URL_PATH%%OPENJDK21_URL_FILE% -P "%DOWNLOAD_DIR%"

:: rename
ren "%DOWNLOAD_DIR%\%OPENJDK21_URL_FILE%" "%FILE%"